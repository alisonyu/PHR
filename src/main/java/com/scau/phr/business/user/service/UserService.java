package com.scau.phr.business.user.service;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.scau.phr.business.auth.domain.Session;
import com.scau.phr.business.auth.service.SimpleSessionServiceImpl;
import com.scau.phr.common.domain.ApiResult;
import com.scau.phr.common.exceptions.ServiceException;
import com.scau.phr.common.util.StrTool;
import com.scau.phr.domain.entity.User;
import com.scau.phr.domain.ienum.Sex;
import com.scau.phr.domain.mapper.UserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.*;

import static com.scau.phr.common.exceptions.ErrorCode.*;

/**
 * 用户信息管理接口
 * Created with IDEA
 * author:scaurb
 * Date:2018/8/15
 * Time:10:00
 **/
@Service
public class UserService{
    @Autowired
    private UserMapper mapper;

    @Autowired
    SimpleSessionServiceImpl sessionService;

    public User queryOne(){
        String telephone = getCurrentTelephone();
        return mapper.queryOne(telephone);
    }

    private String getCurrentTelephone(){
        Session session = sessionService.getCurrentSession();
        String telephone = session.getUsername();
        return telephone;
    }

    /**
     * 可以调整参数
     */
    public void updateUser(String personal_account_id, Integer sex, String location, String email, String nick_name, String education, String job, Integer is_married, String real_name, Integer is_verified, Integer is_vip, String id_card_no){
        User user = new User();
        //主键通过获取当前token得到
        String telephone = getCurrentTelephone();
        user.setTelephone(telephone);

        user.setPersonalAccountId(personal_account_id);
        Sex sex1;
        if(sex == 1){
            sex1 = Sex.MALE;
        }else{
            sex1 = Sex.FEMALE;
        }
        user.setSex(sex1);
        user.setLocation(location);
        user.setEmail(email);
        user.setNickname(nick_name);
        user.setEducation(education);
        user.setJob(job);
        user.setMarry(is_married == 1);
        user.setRealName(real_name);
        user.setVerified(is_verified == 1);
        user.setVip(is_vip == 1);
        user.setIdcard(id_card_no);


        mapper.updateById(user);
    }
}
