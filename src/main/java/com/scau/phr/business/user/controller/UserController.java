package com.scau.phr.business.user.controller;
import com.scau.phr.business.user.service.UserService;
import com.scau.phr.common.domain.ApiResult;
import com.scau.phr.domain.entity.User;
import com.scau.phr.domain.ienum.Sex;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import java.util.List;
import static com.scau.phr.common.exceptions.ErrorCode.SUCCESS;

/**
 * 用户信息管理接口
 * Created with IDEA
 * author:scaurb
 * Date:2018/8/15
 * Time:10:00
 **/
@RestController
@RequestMapping("/user")
public class UserController {

    @Autowired
    private UserService userService;

    /**
     * 根据当前token查询当前用户信息
     * @return
     */
    @RequestMapping(value = "/query")
    public User queryOne(){
        return userService.queryOne();
    }

    /**
     * 根据当前token更新用户信息
     * @param personal_account_id
     * @param sex
     * @param location
     * @param email
     * @param nick_name
     * @param education
     * @param job
     * @param is_married
     * @param real_name
     * @param is_verified
     * @param is_vip
     * @param id_card_no
     */
    @RequestMapping(value = "/update")
    public void updateById(@RequestParam(value = "personal_account_id",defaultValue = "123456") String personal_account_id,
                           @RequestParam(value = "sex",defaultValue = "1") Integer sex,
                           @RequestParam(value = "location",defaultValue = "广东省潮州市") String location,
                           @RequestParam(value = "email",defaultValue = "2237039710@qq.com") String email,
                           @RequestParam(value = "nick_name",defaultValue = "彬") String nick_name,
                           @RequestParam(value = "education",defaultValue = "本科") String education,
                           @RequestParam(value = "job",defaultValue = "学生") String job,
                           @RequestParam(value = "is_married",defaultValue = "0") Integer is_married,
                           @RequestParam(value = "real_name",defaultValue = "锐彬") String real_name,
                           @RequestParam(value = "is_verified",defaultValue = "1") Integer is_verified,
                           @RequestParam(value = "is_vip",defaultValue = "0") Integer is_vip,
                           @RequestParam(value = "id_card_no",defaultValue = "445121199611013156") String id_card_no){
        userService.updateUser(personal_account_id, sex, location, email, nick_name, education, job, is_married, real_name, is_verified, is_vip, id_card_no);
    }


//
//
////    /**
////     * 查找所有表单（分页?）
////     * @return List<ApiResult>
////     */
////    @PostMapping("/queryAll")
////    public List<ApiResult> queryAll(Page page){
////        return userService.queryAll(page);
////    }
//
//    /**
//     * 通过tel查找唯一记录
//     * @param tel
//     * @return ApiResult
//     * 注释：实体类中使用int存储主键，此处使用Integer方便非空检测（修改实体类？）
//     */
//    @PostMapping("/queryOne")
//    public ApiResult queryOne(@RequestParam String tel){
//        return userService.queryOne(tel);
//    }
//
//    /**
//     * 传入user对象插入数据
//     * @param object
//     * @return boolean
//     */
//    @PostMapping("/addItem")
//    public ApiResult addItem(User object){
//        return new ApiResult(SUCCESS,userService.addItem(object));
//    }
//
//    /**
//     * 通过tel删除数据，使用Integer理由同上
//     * @param tel
//     * 注释：暂定返回为votel
//     */
//    @PostMapping("/deleteItem")
//    public ApiResult deleteItem(@RequestParam String tel){
//        userService.deleteItem(tel);
//        return new ApiResult(SUCCESS,true);
//    }
//
//    /**
//     * 传入user对象修改数据
//     * @param object
//     * @return boolean
//     */
//    @PostMapping("/updateItem")
//    public ApiResult updateItem(User object){
//        return new ApiResult(SUCCESS,userService.updateItem(object));
//    }
//
//    /**
//     * 传入user，通过非空成员查询
//     * @param object
//     * @return List<ApiResult>
//     * 未实现
//     */
////    @PostMapping("/Search")
////    public List<ApiResult> Search(Page page,User object){
////        return userService.Search(page,object);
////    }
//
//    /**
//     * 传入user，通过名字模糊查询(分页)
//     * @param name
//     * @return List<ApiResult>
//     */
//    @PostMapping("/SearchByName")
//    public List<ApiResult> SearchByName(String name) {
//        return null;
//    }
}
