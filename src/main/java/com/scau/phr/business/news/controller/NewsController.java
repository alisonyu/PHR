package com.scau.phr.business.news.controller;

import com.scau.phr.business.news.service.NewsService;
import com.scau.phr.domain.entity.HealthKnowledge;
import org.apache.lucene.queryparser.classic.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.util.List;

/**
 * 新闻控制类
 * Created with IDEA
 * author:scaurb
 * Date:2018/8/8/008
 * Time:11:40
 **/
@RestController
@RequestMapping("/news/api")
public class NewsController {
    @Autowired
    private NewsService newsService;

    @RequestMapping(value = "/all")
    public Object listAllHealthKnowledge(@RequestParam(value = "page",defaultValue = "1")Integer page){
        return newsService.ListAllHealthKnowledge(page);
    }

    /**
     * 根据id查询
     * @param id
     * @return
     */
    @RequestMapping(value = "/query")
    public Object findHealthKnowledgeById(@RequestParam("id") Long id){
        return newsService.findHealthKnowledgeById(id);
    }

    /**
     * 根据类型查询
     * @param type
     * @param page
     * @return
     */
    @RequestMapping(value = "/type")
    public Object findHealthKnowledgeBytype(@RequestParam("type") String type,@RequestParam(value = "page",defaultValue = "1")Integer page ){
        return newsService.findHealthKnowledgeBytype(type,page);
    }

    @RequestMapping(value = "/searchByKey")
    public List<HealthKnowledge> findHealthKnowledgeByKeyword(@RequestParam("key") String key) throws IOException, ParseException {
        return newsService.findealthKnowledgeByKeyword(key);
    }
}
