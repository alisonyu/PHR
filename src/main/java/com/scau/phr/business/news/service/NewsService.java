package com.scau.phr.business.news.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.scau.phr.common.SearchEngines.MyLucene;
import com.scau.phr.common.domain.PageDTO;
import com.scau.phr.common.util.PageUtil;
import com.scau.phr.domain.entity.HealthKnowledge;
import com.scau.phr.domain.entity.Newhospital;
import com.scau.phr.domain.mapper.HealthKnowledgeMapper;
import com.scau.phr.domain.mapper.NewsMapper;
import org.apache.lucene.queryparser.classic.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.List;

/**
 * 新闻服务类
 * Created with IDEA
 * author:scaurb
 * Date:2018/8/8/008
 * Time:11:41
 **/
@Service
public class NewsService {
    @Autowired
    private HealthKnowledgeMapper healthKnowledgeMapper;

    @Autowired
    private MyLucene lucene;

    public Object ListAllHealthKnowledge(int current){
        PageDTO res = null;
        if((res = tryGetAllHealthKnowledge(current))!=null){
            return res;
        }else{
            return PageDTO.getEmptyResult();
        }
    }

    /**
     * 根据id查询新闻
     * @param id
     * @return
     */
    public Object findHealthKnowledgeById(Long id){
        HealthKnowledge healthKnowledge = healthKnowledgeMapper.findHealthKnowledgeById(id);
        return healthKnowledge;
    }

    /**
     * '养生保健'(typeA)
     * '疾病科普'(typeB)
     * '养老观察'(typeC)
     * '综合资讯'(typeD)
     * @param type
     * @param current
     * @return
     */
    public Object findHealthKnowledgeBytype(String type,int current){
        PageDTO res =null;
        if(type.equals("A")){
            res = tryFindHealthKnowledgeTypeA(current);
        }else if(type.equals("B")){
            res = tryFindHealthKnowledgeTypeB(current);
        }else if(type.equals("C")){
            res = tryFindHealthKnowledgeTypeC(current);
        }else{
            res = tryFindHealthKnowledgeTypeD(current);
        }
        return res;
    }

    /**
     * 分页显示所有知识（默认问1页10个信息）
     * @param current
     * @return
     */
    private PageDTO tryGetAllHealthKnowledge(int current){
        Page page = PageUtil.getPage(current);
        List<HealthKnowledge> list = healthKnowledgeMapper.listAllHealthKnowledge(page);
        return PageDTO.valueOf(list,page);
    }

    /**
     * '养生保健'(typeA)
     * @param current
     * @return
     */
    private PageDTO tryFindHealthKnowledgeTypeA(int current){
        Page page = PageUtil.getPage(current);
        List<HealthKnowledge> list = healthKnowledgeMapper.findHealthKnowledgeByType("养生保健",page);
        return PageDTO.valueOf(list,page);
    }

    /**
     * '疾病科普'(typeB)
     * @param current
     * @return
     */
    private PageDTO tryFindHealthKnowledgeTypeB(int current){
        Page page = PageUtil.getPage(current);
        List<HealthKnowledge> list = healthKnowledgeMapper.findHealthKnowledgeByType("疾病科普",page);
        return PageDTO.valueOf(list,page);
    }

    /**
     * '养老观察'(typeC)
     * @param current
     * @return
     */
    private PageDTO tryFindHealthKnowledgeTypeC(int current){
        Page page = PageUtil.getPage(current);
        List<HealthKnowledge> list = healthKnowledgeMapper.findHealthKnowledgeByType("养老观察",page);
        return PageDTO.valueOf(list,page);
    }

    /**
     * '综合资讯'(typeD)
     * @param current
     * @return
     */
    private PageDTO tryFindHealthKnowledgeTypeD(int current){
        Page page = PageUtil.getPage(current);
        List<HealthKnowledge> list = healthKnowledgeMapper.findHealthKnowledgeByType("综合资讯",page);
        return PageDTO.valueOf(list,page);
    }

    public List<HealthKnowledge> findealthKnowledgeByKeyword(String key) throws IOException, ParseException {
        //创建索引（只需要执行一次）
        //lucene.createIndexToHealthKnowledge();
        List<HealthKnowledge> list = lucene.searchFromHealthKnowledge("description",key);
        return list;
    }

}
