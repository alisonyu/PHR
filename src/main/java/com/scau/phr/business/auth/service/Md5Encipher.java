package com.scau.phr.business.auth.service;

import com.google.common.hash.Hashing;
import org.springframework.stereotype.Component;

/**
 * MD5加密类
 * @author yuzhiyi
 * @date 2018/7/18 10:30
 */
@Component("Md5Encipher")
public class Md5Encipher implements Encipher{

    //加密盐
    private static final String salt = "d78acuncjkadaiugd%poipda&dhcuioa@;89q2noc";

    /**
     * md5加密
     * @param input 等待加密的字符串
     * @return 返回加密后的字符串
     */
    @Override
    public String encrypt(String input) {
        input = salt + input + salt;
        return Hashing.sha256().hashBytes(input.getBytes()).toString();
    }
}
