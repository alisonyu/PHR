package com.scau.phr.business.auth.utils;

/**
 * 检验手机号
 * @author yuzhiyi
 * @date 2018/7/17 22:05
 */
public class MobileUtil {

    private static final String telRegex = "^((13[0-9])|(14[5,7,9])|(15[^4])|(18[0-9])|(17[0,1,3,5,6,7,8]))\\d{8}$";// "[1]"代表第1位为数字1，"[358]"代表第二位可以为3、5、8中的一个，"\\d{9}"代表后面是可以是0～9的数字，有9位。

    /**
     * 判断字符串是否符合手机号码格式
     * 移动号段: 134,135,136,137,138,139,147,150,151,152,157,158,159,170,178,182,183,184,187,188
     * 联通号段: 130,131,132,145,155,156,170,171,175,176,185,186
     * 电信号段: 133,149,153,170,173,177,180,181,189
     * @param mobileNums 待检测的字符串
     * @return
     */
    public static boolean isMobileNO(String mobileNums) {
        if (mobileNums == null){
            return false;
        }
        mobileNums = mobileNums.trim();
        return mobileNums.matches(telRegex);
    }

}
