package com.scau.phr.business.auth.service;

/**
 * 加密接口
 * @author yuzhiyi
 * @date 2018/7/18 10:29
 */
public interface Encipher {

    public String encrypt(String input);

}
