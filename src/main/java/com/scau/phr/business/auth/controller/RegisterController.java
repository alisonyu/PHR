package com.scau.phr.business.auth.controller;

import com.scau.phr.business.auth.anno.Permitting;
import com.scau.phr.business.auth.domain.Role;
import com.scau.phr.business.auth.domain.Session;
import com.scau.phr.business.auth.domain.TokenResult;
import com.scau.phr.business.auth.service.AuthService;
import com.scau.phr.business.auth.service.SessionService;
import com.scau.phr.business.auth.service.SimpleSessionServiceImpl;
import com.scau.phr.business.auth.service.TokenService;
import com.scau.phr.common.exceptions.ErrorCode;
import com.scau.phr.domain.entity.User;
import com.scau.phr.domain.mapper.UserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDate;

/**
 * 注册控制器
 * @author yuzhiyi
 * @date 2018/7/18 11:10
 */
@RestController
@RequestMapping(value = "/register",method = RequestMethod.POST)
public class RegisterController {


    @Autowired
    private AuthService authService;

    @Autowired
    private UserMapper userMapper;

    @Autowired
    private TokenService tokenService;

    @Autowired
    @Qualifier(SimpleSessionServiceImpl.NAME)
    private SessionService sessionService;

    @Permitting(Role.GUEST)
    @RequestMapping("do")
    public Object register(@RequestParam("phone")String phone,
                           @RequestParam("code")String code,
                           @RequestParam("password") String password,
                           @RequestParam("idCard") String idCard,
                           @RequestParam("realName") String realName){
        //判断身份证是否合法
        if (!authService.isValidIdCard(idCard)){
            throw new IllegalArgumentException("身份证号码不合法");
        }
        //判断该用户是否已经存在
        if (authService.existUser(phone)){
            return ErrorCode.AUTH_USER_EXIST;
        }
        //判断验证码是否正确
        if (!authService.isValidCode(phone,code)){
            return ErrorCode.MSG_CODE_ERROR;
        }
        //对密码进行加密
        String encryptedPassword = authService.encryptPassword(password);
        User user = new User();
        user.setPassword(encryptedPassword);
        user.setTelephone(phone);
        user.setRealName(realName);
        user.setIdcard(idCard);
        user.setVerified(true);
        userMapper.insert(user);
        String token = tokenService.generateToken(phone);
        initSession(token,phone);
        TokenResult result  = new TokenResult();
        result.setToken(token);
        result.setUsername(phone);
        return result;
    }

    private void initSession(String token,String username){
        Session session = new Session(token);
        session.setUsername(username);
        sessionService.addSession(token,session);
    }



}
