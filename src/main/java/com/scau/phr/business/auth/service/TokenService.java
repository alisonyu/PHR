package com.scau.phr.business.auth.service;

import com.google.common.hash.Hashing;
import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.stereotype.Service;

/**
 * 鉴权码服务
 * @author yuzhiyi
 * @date 2018/7/29 17:30
 */
@Service
public class TokenService {

	/**
	 * 生成唯一鉴权Token
	 * @param user 用户名
	 * @return 鉴权Token
	 */
	public String generateToken(String user){
		 String s = RandomStringUtils.random(20);
		 String t = String.valueOf(System.currentTimeMillis());
		 String h = Hashing.crc32().hashBytes(user.getBytes()).toString();
		 return Hashing.sha256().hashUnencodedChars(s+t+h).toString();
	}







}
