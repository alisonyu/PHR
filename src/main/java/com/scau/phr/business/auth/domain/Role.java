package com.scau.phr.business.auth.domain;

/**
 * 具体角色
 * @author yuzhiyi
 * @date 2018/7/29 17:37
 */
public enum Role {
	/**
	 * ALL表示不限角色
	 */
	ALL,
	/**
	 * GUEST表示游客
	 */
	GUEST,
	/**
	 * 注册用户
	 */
	REGISTER_USER,


}
