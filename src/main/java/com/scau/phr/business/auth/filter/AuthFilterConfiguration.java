package com.scau.phr.business.auth.filter;

import com.scau.phr.business.auth.service.SessionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * 权限过滤器配置
 * @author yuzhiyi
 * @date 2018/7/29 18:45
 */
@Configuration
public class AuthFilterConfiguration {

	@Autowired @Qualifier("SimpleSessionService")
	SessionService sessionService;

	@Bean
	@ConditionalOnProperty(prefix = "phr.auth",name="filter-enable",havingValue = "true")
	public FilterRegistrationBean testFilterRegistration(){
		FilterRegistrationBean registration = new FilterRegistrationBean();
		RoleFilter filter = new RoleFilter();
		filter.setSessionService(sessionService);
		registration.setFilter(filter);
		registration.addUrlPatterns("/*");
		registration.setName("RoleFilter");
		return registration;
	}

	@Bean
	public FilterRegistrationBean sessionFilterRegistration(){
		FilterRegistrationBean registration = new FilterRegistrationBean();
		SessionFilter filter = new SessionFilter(sessionService);
		registration.setFilter(filter);
		registration.addUrlPatterns("/*");
		registration.setName("SessionFilter");
		return registration;
	}

}
