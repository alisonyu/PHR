package com.scau.phr.business.auth.service;

import com.scau.phr.business.auth.domain.Session;

/**
 * Session服务接口
 * @author yuzhiyi
 * @date 2018/7/29 17:32
 */
public interface SessionService {

	public Session getSession(String token);

	public void destroySession(String token);

	public Session addSession(String token,Session session);

	//获取当前的Session
	default public Session getCurrentSession(){
		throw new UnsupportedOperationException();
	}

	//设置当前的Session
	default public void setCurrentSession(String token){
		throw new UnsupportedOperationException();
	}

	//token是否有效
	default public boolean validSession(String token){
		return false;
	}

}
