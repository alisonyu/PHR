package com.scau.phr.business.auth.controller;
import com.scau.phr.business.auth.anno.Permitting;
import com.scau.phr.business.auth.domain.Role;
import com.scau.phr.business.auth.domain.Session;
import com.scau.phr.business.auth.domain.TokenResult;
import com.scau.phr.business.auth.service.AuthService;
import com.scau.phr.business.auth.service.SessionService;
import com.scau.phr.business.auth.service.SimpleSessionServiceImpl;
import com.scau.phr.business.auth.service.TokenService;
import com.scau.phr.common.domain.ApiResult;
import com.scau.phr.common.exceptions.ErrorCode;
import com.scau.phr.domain.mapper.UserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * 登录接口控制器
 * @author yuzhiyi
 * @date 2018/7/17 21:45
 */
@RestController
@RequestMapping(value = "/login",method = RequestMethod.POST)
public class LoginController {

    @Autowired
    private AuthService authService;
    @Autowired
    private UserMapper userMapper;
    @Autowired
    private TokenService tokenService;

    @Autowired
    @Qualifier(SimpleSessionServiceImpl.NAME)
    private SessionService sessionService;

    /**
     * 使用验证码登录
     * @param phone 手机号
     * @param code  验证码
     * @return
     */
    @Permitting(Role.GUEST)
    @RequestMapping("/withCode")
    public Object loginWithCode(@RequestParam("phone")String phone,@RequestParam("code")String code){
       if (!authService.existUser(phone)){
           return ApiResult.valueOf(ErrorCode.USER_NOT_REGISTER);
       }
       if(!authService.isValidCode(phone, code)){
           return ApiResult.valueOf(ErrorCode.MSG_CODE_ERROR);
       }
       String token = tokenService.generateToken(phone);
       initSession(token,phone);
       TokenResult result  = new TokenResult();
       result.setToken(token);
       result.setUsername(phone);
       return result;
    }

    //TODO 获取权限信息并实例化Session
    private void initSession(String token,String username){
        Session session = new Session(token);
        session.setUsername(username);
        sessionService.addSession(token,session);
    }

    /**
     * 使用密码登录
     * @param phone 手机号
     * @param password 密码
     * @return
     */
    @Permitting(Role.GUEST)
    @RequestMapping("/withPassword")
    public Object loginWithPassword(@RequestParam("phone") String phone
                                    ,@RequestParam("password")String password){
        //检验用户是否存在
        if (!authService.existUser(phone)){
            return ErrorCode.USER_NOT_REGISTER;
        }
        //检验密码是否正确
        if (!authService.isValidPassword(phone, password)){
            return ErrorCode.AUTH_PASSWORD_ERROR;
        }
        String token = tokenService.generateToken(phone);
        initSession(token,phone);
        TokenResult result  = new TokenResult();
        result.setToken(token);
        result.setUsername(phone);
        return result;
    }

    /**
     * 验证TOKEN是否有效
     */
    @RequestMapping("/validate")
    public Object validateToken(@RequestParam("token")String token){
        return sessionService.validSession(token);
    }



}
