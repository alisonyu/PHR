package com.scau.phr.business.auth.controller;

import com.scau.phr.business.auth.anno.Permitting;
import com.scau.phr.business.auth.domain.Role;
import com.scau.phr.sdk.aliMsg.RandomCodeUtil;
import com.scau.phr.business.auth.service.AuthService;
import com.scau.phr.business.auth.utils.MobileUtil;
import com.scau.phr.common.domain.ApiResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * 短信验证码控制器
 * @author yuzhiyi
 * @date 2018/7/17 22:04
 */
@RestController
@RequestMapping("/messageCode")
public class MessageCodeController {


    @Autowired
    private AuthService authService;

    /**
     * 发送验证码接口
     */
    @Permitting(Role.GUEST)
    @RequestMapping(value = "/get",method = RequestMethod.POST)
    public Object sendCode(@RequestParam("phone")String phone){
        if (!MobileUtil.isMobileNO(phone)){
            throw new IllegalArgumentException("手机号码错误");
        }
        authService.sendVerifyMeseage(phone, RandomCodeUtil.getCode(5));
        return ApiResult.DEFAULT_SUCCESS_RESULT;
    }

}
