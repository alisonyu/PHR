package com.scau.phr.business.auth.filter;

import com.scau.phr.business.auth.domain.Constant;
import com.scau.phr.business.auth.service.SessionService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.*;
import java.io.IOException;

/**
 * @author yuzhiyi
 * @date 2018/8/8 15:12
 */
public class SessionFilter implements Filter {

	private SessionService sessionService;
	private Logger logger = LoggerFactory.getLogger(SessionFilter.class);


	public SessionFilter(SessionService sessionService){
		this.sessionService = sessionService;
	}

	@Override
	public void init(FilterConfig filterConfig) throws ServletException {

	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
		String token = request.getParameter(Constant.TOKEN);
		logger.debug("当前token为:{}",token);
		if (token!=null){
			sessionService.setCurrentSession(token);
		}
		chain.doFilter(request,response);
	}

	@Override
	public void destroy() {

	}

}
