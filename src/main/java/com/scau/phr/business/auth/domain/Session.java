package com.scau.phr.business.auth.domain;

import com.scau.phr.domain.entity.User;

import java.util.Set;

/**
 * Session数据
 * @author yuzhiyi
 * @date 2018/7/29 17:35
 */
public class Session {

	private String token;
	private Role role;
	private Set<Power> powers;
	//这里应该是电话号码
	private String username;

	public Session(String token){
		this.token = token;
	}

	public Role getRole() {
		return role;
	}

	public void setRole(Role role) {
		this.role = role;
	}

	public Set<Power> getPowers() {
		return powers;
	}

	public void setPowers(Set<Power> powers) {
		this.powers = powers;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}
}
