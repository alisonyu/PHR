package com.scau.phr.business.auth.domain;

/**
 * 鉴权码结果
 * @author yuzhiyi
 * @date 2018/7/29 22:30
 */
public class TokenResult {

	private String token;
	private String username;

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}
}
