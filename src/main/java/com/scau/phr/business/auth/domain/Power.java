package com.scau.phr.business.auth.domain;

import java.util.HashMap;
import java.util.Map;

/**
 * 具体权限
 * 序列化的时候只需要序列化code，然后通过code使用valueOf函数反序列化权限
 * @author yuzhiyi
 * @date 2018/7/29 17:37
 */
public enum Power {
	;

	int code;    //权限码
	String desc;  //描述
	private static Map<Integer,Power> powerMap = new HashMap<>();

	static{
		for (Power power:Power.values()){
			powerMap.put(power.code,power);
		}
	}

	Power(int code, String desc) {
		this.code = code;
		this.desc = desc;
	}

	public static Power valueOf(int code){
		return powerMap.get(code);
	}

	public int getCode() {
		return code;
	}

	public String getDesc() {
		return desc;
	}

	@Override
	public String toString() {
		return "Power{" +
				"code=" + code +
				", desc='" + desc + '\'' +
				'}';
	}


}
