package com.scau.phr.business.auth.anno;

import com.scau.phr.business.auth.domain.Power;
import com.scau.phr.business.auth.domain.Role;

import java.lang.annotation.*;

/**
 * 权限管理注解，注解在RestController或者RequestMapping上
 * @author yuzhiyi
 * @date 2018/7/29 17:37
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.METHOD,ElementType.TYPE})
@Documented
public @interface Permitting {
	/**
	 * 允许哪些角色访问接口
	 * 默认所有角色
	 */
	Role[] value() default Role.ALL;

	/**
	 * 拥有什么权限才允许访问接口
	 */
	Power[] power() default {};
}
