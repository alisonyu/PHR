package com.scau.phr.business.map.controller;

import com.scau.phr.common.SearchEngines.MyLucene;
import com.scau.phr.domain.entity.Newhospital;
import org.apache.lucene.search.ScoreDoc;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.util.List;

/**
 * Created with IDEA
 * author:scaurb
 * Date:2018/8/14/014
 * Time:16:33
 **/
@RestController
@RequestMapping(value = "/lucene")
public class MyLuceneController {
    @Autowired
    private MyLucene lucene;

    @RequestMapping("/test")
    public void test() throws IOException {
        //创建索引（第一次运行时执行）
        lucene.createIndexToNewhospital();
        //搜索索引
        lucene.searchToNewhospital();
    }

    @RequestMapping(value = "/query")
    public List<Newhospital> findNewhospitalByField(@RequestParam("searchField") String searchField,@RequestParam("searchValue") String searchValue) throws IOException {
        return lucene.searchFromNewhospital(searchField,searchValue);
    }
}
