package com.scau.phr.business.map.controller;

import com.scau.phr.business.map.Service.HospitalMapService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created with IDEA
 * author:scaurb
 * Date:2018/8/10
 * Time:11:04
 **/
@RestController
@RequestMapping(value = "/map")
public class BaiduApiController {
    @Autowired
    private HospitalMapService mapService;

    @RequestMapping(value = "/getAddress")
    public Object getLocation(@RequestParam("ip") String ip){
		return mapService.getAddressByIp(ip);
    }



}
