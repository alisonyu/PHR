package com.scau.phr.business.map.controller;

import com.scau.phr.business.hosipital.service.HospitalService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created with IDEA
 * author:scaurb
 * Date:2018/8/10/010
 * Time:10:31
 **/
@RestController
public class HospitalMapController {
    @Autowired
    private HospitalService hospitalService;

    @RequestMapping(value = "/test")
    public Object getAddress(){
        return hospitalService.getHospitalLocation("广东省","潮州市");
    }
}
