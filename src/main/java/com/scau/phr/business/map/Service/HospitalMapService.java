package com.scau.phr.business.map.Service;

import com.scau.phr.sdk.baiduMap.BaiduApiService;
import com.scau.phr.sdk.baiduMap.domain.LocationResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created with IDEA
 * author:scaurb
 * Date:2018/8/10
 * Time:10:59
 **/
@Service
public class HospitalMapService {

    @Autowired
    private BaiduApiService baiduApiService;

    public LocationResult getAddressByIp(String ip){
        return baiduApiService.getAddressByIp(ip);
    }


}
