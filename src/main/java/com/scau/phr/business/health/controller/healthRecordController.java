package com.scau.phr.business.health.controller;

import com.scau.phr.business.health.service.healthRecordService;
import com.scau.phr.domain.entity.HealthRecord;
import com.scau.phr.domain.mapper.HealthRecordMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created with IDEA
 * author:scaurb
 * Date:2018/8/14/014
 * Time:9:57
 **/
@RestController
@RequestMapping(value = "/healthRecord/api")
public class healthRecordController {
    @Autowired
    private healthRecordService healthRecordService;

    @RequestMapping(value = "/do")
    public HealthRecord gethealthRecordByPhone(){
        return healthRecordService.gethealthRecordByPhone();
    }

}
