package com.scau.phr.business.health.service;

import com.scau.phr.business.auth.domain.Session;
import com.scau.phr.business.auth.service.SimpleSessionServiceImpl;
import com.scau.phr.domain.entity.HealthRecord;
import com.scau.phr.domain.mapper.HealthRecordMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 健康档案接口
 * Created with IDEA
 * author:scaurb
 * Date:2018/8/14
 * Time:9:38
 **/
@Service
public class healthRecordService {
    @Autowired
    private HealthRecordMapper mapper;

    @Autowired
    SimpleSessionServiceImpl sessionService;

    public HealthRecord gethealthRecordByPhone(){
        String telephone = getCurrentTelephone();
        return mapper.getHealthRecordByPhone(telephone);
    }

    private String getCurrentTelephone(){
        Session session = sessionService.getCurrentSession();
        String telephone = session.getUsername();
        return telephone;
    }

}
