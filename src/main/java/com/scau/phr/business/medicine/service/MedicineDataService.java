package com.scau.phr.business.medicine.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.scau.phr.common.domain.PageDTO;
import com.scau.phr.domain.entity.MedicineData;
import com.scau.phr.domain.mapper.MedicineDataMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

/**
 * @author yuzhiyi
 * @date 2018/8/7 9:59
 */
@Service
public class MedicineDataService {

	Logger logger = LoggerFactory.getLogger(MedicineDataService.class);

	@Autowired
	private MedicineDataMapper medicineDataMapper;

	public Object searchMedicineDataByName(String name,long current){
		Page page = new Page(current,10);
		List<MedicineData> res = medicineDataMapper.searchMedicineByName(name,page);
		logger.debug("查询总数为：{}",page.getSize());
		logger.debug("查询total:{}",page.getTotal());
		return PageDTO.valueOf(res,page);
	}

	/**
	 * 根据ID获取药品详情
	 */
	public Object getMedicineDataDetailById(Integer id){
		return medicineDataMapper.selectById(id);
	}


}
