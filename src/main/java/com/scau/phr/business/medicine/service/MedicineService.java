package com.scau.phr.business.medicine.service;
import com.baomidou.mybatisplus.core.toolkit.IdWorker;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.scau.phr.domain.entity.Medicine;
import com.scau.phr.domain.mapper.MedicineMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

/**
 * 医药相关服务
 * @author yuzhiyi
 * @date 2018/7/29 14:45
 */
@Service
public class MedicineService {
    @Autowired
    private MedicineMapper medicineMapper;
    /**
     * 一页的数据项数
     */
    private static final int PAGE_AMOUNT = 10;
    /**
     * 默认分页
     */
    private static final Page DEFAULT_PAGE = new Page(1,10);


    /**
     * 根据名称查询医药信息
     * @param name 药品名称
     * @param page 当前页码
     * @return  相关医药列表
     */
    public List<Medicine> queryMedicineByNameWithPage(String name,int page){
        Page pagination =getPage(page);
        return medicineMapper.searchByName(name,pagination);
    }

    /**
     * 列出药品目录
     * @param page 当前页数
     * @return 药品的名称
     */
    public List<String> listMedicineName(int page){
        Page page1 = getPage(page);
        return medicineMapper.listMedicineName(page1);
    }

    /**
     * 根据ID获取药品的详细信息
     * @param id 药品ID
     * @return 药品实例
     */
    public Medicine getMedicineById(int id){
        return medicineMapper.selectById(id);
    }


    /**
     * 根据ID删除药品信息
     * @param id 药品ID
     * @return 返回是否删除成功
     */
    public boolean deleteMedicine(int id){
        return medicineMapper.deleteById(id) > 0;
    }

    /**
     * 添加药品信息
     * @param medicine 药品实例
     * @return 药品
     */
    public Medicine addMedicine(Medicine medicine){
        if (medicine.getId() == null){
            medicine.setId(Math.abs((int) IdWorker.getId()));
        }
        medicineMapper.insert(medicine);
        return medicine;
    }

    /**
     * 更新药品信息
     * @param medicine 药品实例
     * @return 是否更新成功
     */
    public boolean updateMedicine(Medicine medicine){
        return medicineMapper.updateById(medicine) > 0;
    }


    private Page getPage(int i){
        if (i<=0){
            return DEFAULT_PAGE;
        }
        return new Page(i,PAGE_AMOUNT);
    }


}
