package com.scau.phr.business.medicine.controller;

import com.scau.phr.business.medicine.service.MedicineDataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author yuzhiyi
 * @date 2018/8/7 10:06
 */
@RestController
@RequestMapping("/medicine/api")
public class MedicineDataController {

	@Autowired
	private MedicineDataService medicineDataService;

	/**
	 * 搜索药品
	 * @param name 名称
	 * @param page 分页
	 * @return
	 */
	@RequestMapping("/search")
	public Object searchMedicine(@RequestParam("name")String name,@RequestParam(value = "page",defaultValue = "1") long page){
		return medicineDataService.searchMedicineDataByName(name,page);
	}

	/**
	 * 药品详情
	 * @param id 药品ID
	 */
	@RequestMapping("/detail")
	public Object getMedicineDetail(@RequestParam("id")Integer id){
		return medicineDataService.getMedicineDataDetailById(id);
	}

}
