package com.scau.phr.business.bmi.service;

import com.scau.phr.business.bmi.domain.BmiDTO;
import com.scau.phr.domain.entity.PhrBmi;
import com.scau.phr.domain.mapper.PhrBmiMapper;
import com.scau.phr.domain.mapper.UserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;

/**
 * @author yuzhiyi
 * @date 2018/8/14 10:02
 */
@Service
public class BmiService {

	@Autowired
	PhrBmiMapper phrBmiMapper;

	@Autowired
	UserMapper userMapper;


	public Object getWeightHistory(String tel){
		Integer height = userMapper.getHeightByTel(tel);
		List<PhrBmi> weights = phrBmiMapper.listWeightHistory(tel);
		BmiDTO dto = new BmiDTO();
		dto.setHeight(height);
		dto.setWeights(weights);
		return dto;
	}

	//添加体重记录
	public void addWeightRecord(String tel,Double weight){
		PhrBmi record = new PhrBmi();
		record.setTel(tel);
		record.setWeight(weight);
		record.setTime(LocalDateTime.now());
		phrBmiMapper.insert(record);
	}

	//添加体重记录
	public void addWeightRecord(String tel,Double weight,LocalDateTime dateTime){
		PhrBmi record = new PhrBmi();
		record.setTime(dateTime);
		record.setTel(tel);
		record.setWeight(weight);
		phrBmiMapper.insert(record);
	}

	//更新体重信息
	public void updateHeight(String tel,Integer height){
		userMapper.updateHeight(tel,height);
	}


}
