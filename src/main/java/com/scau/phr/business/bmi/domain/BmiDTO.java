package com.scau.phr.business.bmi.domain;

import com.scau.phr.domain.entity.PhrBmi;

import java.util.List;

/**
 * Bmi数据DTO
 * @author yuzhiyi
 * @date 2018/8/15 10:02
 */
public class BmiDTO {

	private Integer height;
	private List<PhrBmi> weights;

	public Integer getHeight() {
		return height;
	}

	public void setHeight(Integer height) {
		this.height = height;
	}

	public List<PhrBmi> getWeights() {
		return weights;
	}

	public void setWeights(List<PhrBmi> weights) {
		this.weights = weights;
	}

	@Override
	public String toString() {
		return "BmiDTO{" +
				"height=" + height +
				", weights=" + weights +
				'}';
	}
}
