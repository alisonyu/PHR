package com.scau.phr.business.hosipital.domain;

import com.scau.phr.domain.entity.Appointment;

import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

/**
 * 申请预约DTO
 * @author yuzhiyi
 * @date 2018/8/7 16:13
 */
public class AppointmentApplyDto {


	@NotNull(message = "hospitalId不能为空")
	private Long hospitalId;
	@NotNull(message = "科室名称不能为空")
	private String department;
	@NotNull(message = "医生名称不能为空")
	private String doctor;
	@NotNull(message = "预约时间不能为空")
	private String startDate;

	public Appointment wrap(String username){
		Appointment appointment = new Appointment();
		appointment.setUserId(username);
		appointment.setDepartment(this.department);
		appointment.setDoctor(this.doctor);
		appointment.setHospitalId(hospitalId);
		appointment.setStartTime(LocalDate.parse(startDate, DateTimeFormatter.ISO_DATE));
		appointment.setStopTime(appointment.getStartTime().plusDays(1));
		return appointment;
	}



	public Long getHospitalId() {
		return hospitalId;
	}

	public void setHospitalId(Long hospitalId) {
		this.hospitalId = hospitalId;
	}

	public String getDepartment() {
		return department;
	}

	public void setDepartment(String department) {
		this.department = department;
	}

	public String getDoctor() {
		return doctor;
	}

	public void setDoctor(String doctor) {
		this.doctor = doctor;
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}
}
