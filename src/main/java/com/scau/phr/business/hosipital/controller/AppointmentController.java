package com.scau.phr.business.hosipital.controller;

import com.scau.phr.business.hosipital.domain.AppointmentApplyDto;
import com.scau.phr.business.hosipital.service.AppointmentService;
import com.scau.phr.domain.entity.Appointment;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

/**
 * 医院预约Controller
 * @author yuzhiyi
 * @date 2018/8/7 15:56
 */
@RestController
@RequestMapping(value = "/appointment/api")
public class AppointmentController {

	@Autowired
	private AppointmentService appointmentService;

	@RequestMapping("/do")
	public Object appoint(@Valid AppointmentApplyDto apply){
		return appointmentService.doAppointment(apply);
	}


	@RequestMapping("/get")
	public Object getAppointmentHistory(@RequestParam("token") String token){
		return appointmentService.getAppointmentHistory(token);
	}



}
