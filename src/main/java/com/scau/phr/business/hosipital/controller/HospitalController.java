package com.scau.phr.business.hosipital.controller;

import com.scau.phr.business.hosipital.service.HospitalService;
import com.scau.phr.business.hosipital.util.NetworkUtil;
import com.scau.phr.sdk.baiduMap.MyAddressUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

/**
 * 医院API相关接口
 * @author yuzhiyi
 * @date 2018/8/7 15:09
 */
@RestController
@RequestMapping("/hospital/api")
public class HospitalController {

	@Autowired
	private HospitalService hospitalService;

	@Autowired
	private MyAddressUtil myAddressUtil;

	@RequestMapping("/query")
	public Object searchHospital(@RequestParam("provice") String provice,
							  @RequestParam("city") String city,
							  @RequestParam("county") String county,
							  @RequestParam("page") Integer page
							  ) {
		return hospitalService.getHospital(provice,city,county,page);
	}


	@RequestMapping("/detail")
	public Object getHospitalDetail(@RequestParam("hospitalId")Long id ){
		return hospitalService.getHospitalDetail(id);
	}

	@RequestMapping("/near")
	public Object getNearHospital(@RequestParam("city") String city,
								  @RequestParam("longitude") double longitude,
								  @RequestParam("latitude") double latitude){
		return myAddressUtil.getNearHospital(city,longitude,latitude);
	}

	/**
	 * 获取IP地址范例
	 * 但是本机测试只能拿到127.0.0.1
	 */
	@RequestMapping("/test")
	public Object test(HttpServletRequest request) throws IOException {
		System.out.println(NetworkUtil.getIpAddress(request));
		return true;
	}

}
