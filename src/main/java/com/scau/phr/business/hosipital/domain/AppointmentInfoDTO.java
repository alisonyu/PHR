package com.scau.phr.business.hosipital.domain;

import com.scau.phr.domain.entity.Appointment;

import java.time.LocalDate;

/**
 * 预约历史记录
 * @author yuzhiyi
 * @date 2018/8/8 14:56
 */
public class AppointmentInfoDTO {

	//用户手机号
	private String phone;
	//医院名称
	private String hospitalName;
	//医院地址
	private String hospitalAddress;
	//医院电话
	private String hospitalTel;
	//预约科室
	private String department;
	//预约医生名称
	private String doctor;
	//预约开始时间
	private LocalDate startTime;
	//预约结束时间
	private LocalDate stopTime;

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getHospitalName() {
		return hospitalName;
	}

	public void setHospitalName(String hospitalName) {
		this.hospitalName = hospitalName;
	}

	public String getHospitalAddress() {
		return hospitalAddress;
	}

	public void setHospitalAddress(String hospitalAddress) {
		this.hospitalAddress = hospitalAddress;
	}

	public String getHospitalTel() {
		return hospitalTel;
	}

	public void setHospitalTel(String hospitalTel) {
		this.hospitalTel = hospitalTel;
	}

	public String getDepartment() {
		return department;
	}

	public void setDepartment(String department) {
		this.department = department;
	}

	public String getDoctor() {
		return doctor;
	}

	public void setDoctor(String doctor) {
		this.doctor = doctor;
	}

	public LocalDate getStartTime() {
		return startTime;
	}

	public void setStartTime(LocalDate startTime) {
		this.startTime = startTime;
	}

	public LocalDate getStopTime() {
		return stopTime;
	}

	public void setStopTime(LocalDate stopTime) {
		this.stopTime = stopTime;
	}

	@Override
	public String toString() {
		return "AppointmentInfoDTO{" +
				"phone='" + phone + '\'' +
				", hospitalName='" + hospitalName + '\'' +
				", hospitalAddress='" + hospitalAddress + '\'' +
				", hospitalTel='" + hospitalTel + '\'' +
				", department='" + department + '\'' +
				", doctor='" + doctor + '\'' +
				", startTime=" + startTime +
				", stopTime=" + stopTime +
				'}';
	}
}
