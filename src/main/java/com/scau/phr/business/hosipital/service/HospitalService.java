package com.scau.phr.business.hosipital.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.google.common.cache.Cache;
import com.scau.phr.common.domain.PageDTO;
import com.scau.phr.common.util.PageUtil;
import com.scau.phr.domain.entity.Newhospital;
import com.scau.phr.domain.mapper.NewhospitalMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.List;

/**
 * @author yuzhiyi
 * @date 2018/8/7 10:45
 */
@Service
public class HospitalService {

	Logger logger = LoggerFactory.getLogger(HospitalService.class);

	@Autowired
	private NewhospitalMapper mapper;

	//TODO 考虑添加缓存
	private Cache<String,Boolean> invalidAddressCache;

	@PostConstruct
	public void init(){
		//初始化非法地点的缓存
//		invalidAddressCache = CacheBuilder
//				.newBuilder()
//				.softValues()
//				.build();
	}

	/**
	 * 获取医院信息
	 * 当找不到相关县数据的时候会尝试查找市的数据,
	 * 找不到市的相关数据会尝试查找省的数据，
	 * 假如没有省的数据会通过Logger记录
	 * @param provice 省
	 * @param city 市
	 * @param county 县
	 */
	public Object getHospital(String provice,String city,String county,int page){
		PageDTO res = null;
		if ((res = tryGetCountyHospital(county,provice,page))!=null){
			logger.debug("{},{},{}尝试从{}获取数据成功",provice,city,county,county);
			return res;
		}
		else if ((res = tryGetCityHospital(city,page))!=null){
			logger.debug("{},{},{}尝试从{}获取数据成功",provice,city,county,city);
			return res;
		}
		else if ((res = tryGetProviceHospital(provice,page))!=null){
			logger.debug("{},{},{}尝试从{}获取数据成功",provice,city,county,provice);
			return res;
		}
		else{
			logger.debug("{},{},{}获取数据失败",provice,city,county);
			return PageDTO.getEmptyResult();
		}
	}

	public Newhospital getHospitalDetail(Long id){
		return mapper.selectById(id);
	}

	private PageDTO tryGetCountyHospital(String county,String provice,int current){
		Page page = PageUtil.getPage(current);
		List<Newhospital> list = mapper.listHospitalByCounty(county,provice,page);
		if (page.getTotal() == null || page.getTotal() == 0){
			return null;
		}
		return PageDTO.valueOf(list,page);
	}

	private PageDTO tryGetCityHospital(String city,int current){
		Page page = PageUtil.getPage(current);
		List<Newhospital> list = mapper.listHospitalByCity(city,page);
		if (page.getTotal() == null || page.getTotal() == 0){
			return null;
		}
		return PageDTO.valueOf(list,page);
	}

	private PageDTO tryGetProviceHospital(String provice,int current){
		Page page = PageUtil.getPage(current);
		List<Newhospital> list = mapper.listHospitialByProvice(provice,page);
		if (page.getTotal() == null || page.getTotal() == 0){
			return null;
		}
		return PageDTO.valueOf(list,page);
	}

	/**
	 * 根据省跟市拉取数据
	 * @param province
	 * @param city
	 * @return
	 */
	public List<Newhospital> getHospitalLocation(String province,String city){
		List<Newhospital> list = mapper.getHospitalLocation(province,city);
		return list;
	}

	public List<Newhospital> getHospitalLocation2(String city){
		List<Newhospital> list = mapper.getHospitalLocation2(city);
		return list;
	}

}
