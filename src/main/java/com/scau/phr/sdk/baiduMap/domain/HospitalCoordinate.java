package com.scau.phr.sdk.baiduMap.domain;

/**
 * 用户表示医院的地理坐标位置(经纬度)
 * Created with IDEA
 * author:scaurb
 * Date:2018/8/13 10:27
 * Time:10:24
 **/
public class HospitalCoordinate implements Comparable<HospitalCoordinate>{
    //经度
    private double longitude;
    //维度
    private double latitude;
    //距离，作为排序依据
    private double dist;

    public double getDist() {
        return dist;
    }

    public void setDist(double dist) {
        this.dist = dist;
    }

    public double getLongitude() {
        return longitude;
    }
    public HospitalCoordinate(){

    }
    public HospitalCoordinate(double x,double y){
        this.longitude = x;
        this.latitude = y;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }


    @Override
    public String toString() {
        return "HospitalCoordinate{" +
                "longitude=" + longitude +
                ", latitude=" + latitude +
                ", dist=" + dist +
                '}';
    }

    @Override
    public int compareTo(HospitalCoordinate o) {
        HospitalCoordinate p = (HospitalCoordinate)o;
        if(this.dist < p.dist){
            return -1;
        }else if(this.dist > p.dist){
            return 1;
        }else {
            return 0;
        }

    }

}
