package com.scau.phr.sdk.baiduMap;

import com.scau.phr.sdk.baiduMap.domain.LocationResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.configurationprocessor.json.JSONException;
import org.springframework.boot.configurationprocessor.json.JSONObject;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLConnection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 调用百度api实现ip定位或，定位经纬度
 * Created with IDEA
 * author:scaurb
 * Date:2018/8/9
 * Time:11:09
 **/
@Component
public class BaiduApiService {

    private static final String LOCATION_API_URL = "http://api.map.baidu.com/location/ip";
    private static final String AK = "OglDSmakHcYgw3MHcNMIEvB1Qfd3P1o9";

    @Autowired
    private RestTemplate restTemplate;

    public LocationResult getAddressByIp(String ip){
        String url = getLocationRequestUrl(ip);
        Map<String,Object> res  = restTemplate.getForObject(url,Map.class);
        System.out.println(res);
        return LocationResult.valueOf(res);
    }

    private String getLocationRequestUrl(String ip){
        return LOCATION_API_URL + "?" + "ip=" + ip + "&ak=" +AK;
    }




}
