package com.scau.phr.sdk.baiduMap.domain;

import com.scau.phr.common.exceptions.ErrorCode;
import com.scau.phr.common.exceptions.ServiceException;

import java.util.Map;

/**
 * @author yuzhiyi
 * @date 2018/8/10 23:01
 */
public class LocationResult {

	private String province;
	private String city;
	private String district;
	private String address;
	private double x;
	private double y;

	public static LocationResult valueOf(Map<String,Object> map){
		try{
			LocationResult locationResult = new LocationResult();
			Map<String,Object> content = (Map<String, Object>) map.get("content");
			String address = (String) content.get("address");
			locationResult.setAddress(address);
			Map<String,String> pos = (Map<String, String>) content.get("point");
			locationResult.x = Double.parseDouble(pos.get("x"));
			locationResult.y = Double.parseDouble(pos.get("y"));
			Map<String,String> detail = (Map<String, String>) content.get("address_detail");
			locationResult.province = detail.get("province");
			locationResult.city = detail.get("city");
			locationResult.district = detail.get("district");
			return locationResult;
		}catch (Exception e){
			e.printStackTrace();
			throw new ServiceException(ErrorCode.SERVER_ERROR,"根据IP获取地址转换失败");
		}
	}


	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public double getX() {
		return x;
	}

	public void setX(double x) {
		this.x = x;
	}

	public double getY() {
		return y;
	}

	public void setY(double y) {
		this.y = y;
	}

	public String getProvince() {
		return province;
	}

	public void setProvince(String province) {
		this.province = province;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getDistrict() {
		return district;
	}

	public void setDistrict(String district) {
		this.district = district;
	}

	@Override
	public String toString() {
		return "LocationResult{" +
				"address='" + address + '\'' +
				", x=" + x +
				", y=" + y +
				'}';
	}
}
