package com.scau.phr.sdk.aliMsg;

import org.apache.commons.lang3.RandomUtils;

/**
 * 用于生成N位的验证码
 * @author yuzhiyi
 * @date 2018/7/17 16:55
 */
public class RandomCodeUtil {

    public static String getCode(int n){
        n = n>0?n:5;
        StringBuilder sb = new StringBuilder();
        for (int i=0;i<n;i++){
            sb.append(RandomUtils.nextInt()%10);
        }
        return sb.toString();
    }

}
