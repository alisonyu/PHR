package com.scau.phr.sdk.aliMsg;
import com.aliyuncs.DefaultAcsClient;
import com.aliyuncs.IAcsClient;
import com.aliyuncs.dysmsapi.model.v20170525.SendSmsRequest;
import com.aliyuncs.dysmsapi.model.v20170525.SendSmsResponse;
import com.aliyuncs.exceptions.ClientException;
import com.aliyuncs.http.MethodType;
import com.aliyuncs.profile.DefaultProfile;
import com.aliyuncs.profile.IClientProfile;
import com.scau.phr.common.exceptions.ErrorCode;
import com.scau.phr.common.exceptions.ServiceException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

/**
 * 用于发送阿里云短信
 * @author yuzhiyi
 * @date 2018/7/17 15:50
 */
@Component
public class AliMessageService {

    @Autowired
    AliyunMessageConfig aliyunMessageConfig;

    private static final Logger logger = LoggerFactory.getLogger(AliMessageService.class.getName());
    //初始化ascClient需要的几个参数
    private static final String product = "Dysmsapi";//短信API产品名称（短信产品名固定，无需修改）
    private static final String domain = "dysmsapi.aliyuncs.com";//短信API产品域名（接口地址固定，无需修改）
    private String accessKeyId ;//你的accessKeyId,参考本文档步骤2
    private String accessKeySecret;//你的accessKeySecret，参考本文档步骤2
    //初始化ascClient,暂时不支持多region（请勿修改）
    private  IClientProfile profile;
    //短信客户端
    private IAcsClient acsClient;
    //短信模板
    private static final String tplCode = "SMS_139955056";
    //个人开发者只能申请实名，暂时如此
    private static final String signName = "余志毅";

    public AliMessageService(){
        System.setProperty("sun.net.client.defaultConnectTimeout", "10000");
        System.setProperty("sun.net.client.defaultReadTimeout", "10000");
    }

    /**
     * 配置注入之后执行该方法
     * 初始化Clinet
     */
    @PostConstruct
    public void init(){
        accessKeyId = aliyunMessageConfig.getAccessKeyId();
        accessKeySecret = aliyunMessageConfig.getAccessKeySecret();
        IClientProfile profile = DefaultProfile.getProfile("cn-hangzhou", accessKeyId, accessKeySecret);
        try {
            DefaultProfile.addEndpoint("cn-hangzhou", "cn-hangzhou", product, domain);
        } catch (ClientException e) {
            e.printStackTrace();
        }
        this.profile = profile;
        this.acsClient = new DefaultAcsClient(profile);
    }

    /**
     * 发送短信验证码
     * 由于该方法会一直等待到对方收到信息，因此将该方法设置为异步方法
     * @param phoneNumber 目标电话号码
     * @param content 短信内容
     * @throws ServiceException 如果发送短信失败将直接抛出业务逻辑异常
     */
    @Async
    public void send(String phoneNumber,String content){
        SendSmsRequest request = createRequest(phoneNumber,content);
        //请求失败这里会抛ClientException异常
        SendSmsResponse sendSmsResponse = null;
        try {
            sendSmsResponse = acsClient.getAcsResponse(request);
        }
        //如果发送失败将抛出业务逻辑异常
        catch (ClientException e) {
           logger.error(e.getMessage());
           e.printStackTrace();
           throw new ServiceException(ErrorCode.MSG_REQ_ERROR);
        }
        if(sendSmsResponse.getCode() == null || !sendSmsResponse.getCode().equals("OK")) {
            logger.warn("短信服务异常，返回码为："+String.valueOf(sendSmsResponse.getCode()));
            throw new ServiceException(ErrorCode.MSG_REQ_ERROR);
        }
    }

    public String send(String telphone){
        String code = RandomCodeUtil.getCode(5);
        send(telphone,code);
        return code;
    }


    private SendSmsRequest createRequest(String phoneNumber, String code){
        SendSmsRequest request = new SendSmsRequest();
        //使用post提交
        request.setMethod(MethodType.POST);
        //必填:待发送手机号。支持以逗号分隔的形式进行批量调用，批量上限为1000个手机号码,批量调用相对于单条调用及时性稍有延迟,验证码类型的短信推荐使用单条调用的方式；发送国际/港澳台消息时，接收号码格式为00+国际区号+号码，如“0085200000000”
        request.setPhoneNumbers(phoneNumber);
        //必填:短信签名-可在短信控制台中找到
        request.setSignName(signName);
        //必填:短信模板-可在短信控制台中找到
        request.setTemplateCode(tplCode);
        //可选:模板中的变量替换JSON串,如模板内容为"亲爱的${name},您的验证码为${code}"时,此处的值为
        //友情提示:如果JSON中需要带换行符,请参照标准的JSON协议对换行符的要求,比如短信内容中包含\r\n的情况在JSON中需要表示成\\r\\n,否则会导致JSON在服务端解析失败
        request.setTemplateParam(String.format("{\"code\":\"%s\"}",code));
        //可选-上行短信扩展码(扩展码字段控制在7位或以下，无特殊需求用户请忽略此字段)
        //request.setSmsUpExtendCode("90997");
        //可选:outId为提供给业务方扩展字段,最终在短信回执消息中将此值带回给调用者
        //request.setOutId("yourOutId");
        return request;
    }



}
