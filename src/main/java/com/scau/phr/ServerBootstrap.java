package com.scau.phr;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.Profile;
import org.springframework.core.task.AsyncTaskExecutor;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import java.util.concurrent.Executors;


/**
 * 服务端启动类
 * @author yuzhiyi
 * @date 2018/7/9 10:51
 */
@EnableWebMvc
@EnableAutoConfiguration
@EnableAsync
@ComponentScan(basePackages = "com.scau.phr")
@MapperScan(basePackages = "com.scau.phr")
public class ServerBootstrap {

    public static void main(String[] args){
        SpringApplication.run(ServerBootstrap.class,args);
        //开启服务器之后GC一次，减少内存占用
        System.gc();
    }

    /**
     * 异步线程池
     * @return
     */
    @Bean
    @Profile(value = {"prod","dev"})
    public AsyncTaskExecutor teskExecutor(){
        ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
        executor.setThreadNamePrefix("async-");
        executor.setMaxPoolSize(5);
        return executor;
    }


    @Bean
    public RestTemplate restTemplate(){
        return new RestTemplate();
    }

}
