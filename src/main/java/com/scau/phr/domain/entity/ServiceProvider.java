package com.scau.phr.domain.entity;

import java.time.LocalDateTime;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.FieldFill;;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.IdType;
import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author yuzhiyi
 * @since 2018-08-05
 */
public class ServiceProvider implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "provider_id", type = IdType.NONE)
    private String providerId;
    @TableField("provider_name")
    private String providerName;
    @TableField("provider_property")
    private Integer providerProperty;
    @TableField(value = "create_time", fill = FieldFill.INSERT)
    private LocalDateTime createTime;
    @TableField(value = "update_time", fill = FieldFill.INSERT_UPDATE)
    private LocalDateTime updateTime;
    @TableField("manager_name")
    private String managerName;
    @TableField("manager_idcard")
    private String managerIdcard;
    @TableField("address")
    private String address;
    @TableField("service_content")
    private String serviceContent;
    @TableField("start_time")
    private LocalDateTime startTime;
    @TableField("stop_time")
    private LocalDateTime stopTime;
    @TableField("remark")
    private String remark;


    public String getProviderId() {
        return providerId;
    }

    public void setProviderId(String providerId) {
        this.providerId = providerId;
    }

    public String getProviderName() {
        return providerName;
    }

    public void setProviderName(String providerName) {
        this.providerName = providerName;
    }

    public Integer getProviderProperty() {
        return providerProperty;
    }

    public void setProviderProperty(Integer providerProperty) {
        this.providerProperty = providerProperty;
    }

    public LocalDateTime getCreateTime() {
        return createTime;
    }

    public void setCreateTime(LocalDateTime createTime) {
        this.createTime = createTime;
    }

    public LocalDateTime getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(LocalDateTime updateTime) {
        this.updateTime = updateTime;
    }

    public String getManagerName() {
        return managerName;
    }

    public void setManagerName(String managerName) {
        this.managerName = managerName;
    }

    public String getManagerIdcard() {
        return managerIdcard;
    }

    public void setManagerIdcard(String managerIdcard) {
        this.managerIdcard = managerIdcard;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getServiceContent() {
        return serviceContent;
    }

    public void setServiceContent(String serviceContent) {
        this.serviceContent = serviceContent;
    }

    public LocalDateTime getStartTime() {
        return startTime;
    }

    public void setStartTime(LocalDateTime startTime) {
        this.startTime = startTime;
    }

    public LocalDateTime getStopTime() {
        return stopTime;
    }

    public void setStopTime(LocalDateTime stopTime) {
        this.stopTime = stopTime;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    @Override
    public String toString() {
        return "ServiceProvider{" +
        ", providerId=" + providerId +
        ", providerName=" + providerName +
        ", providerProperty=" + providerProperty +
        ", createTime=" + createTime +
        ", updateTime=" + updateTime +
        ", managerName=" + managerName +
        ", managerIdcard=" + managerIdcard +
        ", address=" + address +
        ", serviceContent=" + serviceContent +
        ", startTime=" + startTime +
        ", stopTime=" + stopTime +
        ", remark=" + remark +
        "}";
    }
}
