package com.scau.phr.domain.entity;

import java.time.LocalDate;
import java.time.LocalDateTime;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.FieldFill;;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.IdType;
import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author yuzhiyi
 * @since 2018-08-05
 */
public class Doctor implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "doctor_id", type = IdType.AUTO)
    private Long doctorId;
    @TableField("telephone")
    private String telephone;
    @TableField("password")
    private String password;
    @TableField("real_name")
    private String realName;
    @TableField("sex")
    private String sex;
    @TableField("registered_date")
    private LocalDate registeredDate;
    @TableField("provider_id")
    private Long providerId;
    @TableField("department_id")
    private Long departmentId;
    @TableField("is_verify")
    private Boolean verify;
    @TableField("email")
    private String email;
    @TableField("nick_name")
    private String nickName;
    @TableField("job")
    private String job;
    @TableField("certificateId")
    private String certificateId;
    @TableField("experience")
    private Integer experience;
    @TableField("introduction")
    private String introduction;
    @TableField("workinglocation")
    private String workinglocation;
    @TableField("education")
    private String education;
    @TableField("title")
    private String title;
    @TableField(value = "create_time", fill = FieldFill.INSERT)
    private LocalDateTime createTime;
    @TableField(value = "update_time", fill = FieldFill.INSERT_UPDATE)
    private LocalDateTime updateTime;


    public Long getDoctorId() {
        return doctorId;
    }

    public void setDoctorId(Long doctorId) {
        this.doctorId = doctorId;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getRealName() {
        return realName;
    }

    public void setRealName(String realName) {
        this.realName = realName;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public LocalDate getRegisteredDate() {
        return registeredDate;
    }

    public void setRegisteredDate(LocalDate registeredDate) {
        this.registeredDate = registeredDate;
    }

    public Long getProviderId() {
        return providerId;
    }

    public void setProviderId(Long providerId) {
        this.providerId = providerId;
    }

    public Long getDepartmentId() {
        return departmentId;
    }

    public void setDepartmentId(Long departmentId) {
        this.departmentId = departmentId;
    }

    public Boolean getVerify() {
        return verify;
    }

    public void setVerify(Boolean verify) {
        this.verify = verify;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public String getJob() {
        return job;
    }

    public void setJob(String job) {
        this.job = job;
    }

    public String getCertificateId() {
        return certificateId;
    }

    public void setCertificateId(String certificateId) {
        this.certificateId = certificateId;
    }

    public Integer getExperience() {
        return experience;
    }

    public void setExperience(Integer experience) {
        this.experience = experience;
    }

    public String getIntroduction() {
        return introduction;
    }

    public void setIntroduction(String introduction) {
        this.introduction = introduction;
    }

    public String getWorkinglocation() {
        return workinglocation;
    }

    public void setWorkinglocation(String workinglocation) {
        this.workinglocation = workinglocation;
    }

    public String getEducation() {
        return education;
    }

    public void setEducation(String education) {
        this.education = education;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public LocalDateTime getCreateTime() {
        return createTime;
    }

    public void setCreateTime(LocalDateTime createTime) {
        this.createTime = createTime;
    }

    public LocalDateTime getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(LocalDateTime updateTime) {
        this.updateTime = updateTime;
    }

    @Override
    public String toString() {
        return "Doctor{" +
        ", doctorId=" + doctorId +
        ", telephone=" + telephone +
        ", password=" + password +
        ", realName=" + realName +
        ", sex=" + sex +
        ", registeredDate=" + registeredDate +
        ", providerId=" + providerId +
        ", departmentId=" + departmentId +
        ", verify=" + verify +
        ", email=" + email +
        ", nickName=" + nickName +
        ", job=" + job +
        ", certificateId=" + certificateId +
        ", experience=" + experience +
        ", introduction=" + introduction +
        ", workinglocation=" + workinglocation +
        ", education=" + education +
        ", title=" + title +
        ", createTime=" + createTime +
        ", updateTime=" + updateTime +
        "}";
    }
}
