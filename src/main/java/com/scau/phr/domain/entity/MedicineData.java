package com.scau.phr.domain.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.IdType;
import java.io.Serializable;

/**
 * 药品信息类
 * @author yuzhiyi
 * @since 2018-08-07
 */
public class MedicineData implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.NONE)
    private Integer id;
    @TableField("name")
    private String name;
    @TableField("image")
    private String image;
    @TableField("composition")
    private String composition;
    @TableField("efficacy")
    private String efficacy;
    @TableField("medicine_usage")
    private String medicineUsage;
    @TableField("contraindication")
    private String contraindication;
    @TableField("price")
    private String price;
    @TableField("insuranceType")
    private String insuranceType;
    @TableField("medicineType")
    private String medicineType;
    @TableField("attention")
    private String attention;
    @TableField("producer")
    private String producer;
    @TableField("permit_id")
    private String permitId;
    @TableField("size")
    private String size;
    @TableField("store")
    private String store;
    @TableField("validity_time")
    private String validityTime;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getComposition() {
        return composition;
    }

    public void setComposition(String composition) {
        this.composition = composition;
    }

    public String getEfficacy() {
        return efficacy;
    }

    public void setEfficacy(String efficacy) {
        this.efficacy = efficacy;
    }

    public String getMedicineUsage() {
        return medicineUsage;
    }

    public void setMedicineUsage(String medicineUsage) {
        this.medicineUsage = medicineUsage;
    }

    public String getContraindication() {
        return contraindication;
    }

    public void setContraindication(String contraindication) {
        this.contraindication = contraindication;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getInsuranceType() {
        return insuranceType;
    }

    public void setInsuranceType(String insuranceType) {
        this.insuranceType = insuranceType;
    }

    public String getMedicineType() {
        return medicineType;
    }

    public void setMedicineType(String medicineType) {
        this.medicineType = medicineType;
    }

    public String getAttention() {
        return attention;
    }

    public void setAttention(String attention) {
        this.attention = attention;
    }

    public String getProducer() {
        return producer;
    }

    public void setProducer(String producer) {
        this.producer = producer;
    }

    public String getPermitId() {
        return permitId;
    }

    public void setPermitId(String permitId) {
        this.permitId = permitId;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public String getStore() {
        return store;
    }

    public void setStore(String store) {
        this.store = store;
    }

    public String getValidityTime() {
        return validityTime;
    }

    public void setValidityTime(String validityTime) {
        this.validityTime = validityTime;
    }

    @Override
    public String toString() {
        return "MedicineData{" +
        ", id=" + id +
        ", name=" + name +
        ", image=" + image +
        ", composition=" + composition +
        ", efficacy=" + efficacy +
        ", medicineUsage=" + medicineUsage +
        ", contraindication=" + contraindication +
        ", price=" + price +
        ", insuranceType=" + insuranceType +
        ", medicineType=" + medicineType +
        ", attention=" + attention +
        ", producer=" + producer +
        ", permitId=" + permitId +
        ", size=" + size +
        ", store=" + store +
        ", validityTime=" + validityTime +
        "}";
    }
}
