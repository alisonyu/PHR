package com.scau.phr.domain.entity;

import java.time.LocalDate;
import java.time.LocalDateTime;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.IdType;
import java.io.Serializable;

/**
 *
 *  在线挂号记录
 * @author yuzhiyi
 * @since 2018-08-05
 */
public class Appointment implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "appointment_id", type = IdType.NONE)
    private Long appointmentId;
    @TableField(value = "user_id")
    private String userId;
    @TableField("hospital_id")
    private Long hospitalId;
    @TableField("department")
    private String department;
    @TableField("doctor")
    private String doctor;
    @TableField("stop_time")
    private LocalDate stopTime;
    @TableField("start_time")
    private LocalDate startTime;
    @TableField(value = "create_time", fill = FieldFill.INSERT)
    private LocalDate createTime;
    @TableField(value = "update_time",fill=FieldFill.INSERT_UPDATE)
    private LocalDate updateTime;


    public Long getAppointmentId() {
        return appointmentId;
    }

    public void setAppointmentId(Long appointmentId) {
        this.appointmentId = appointmentId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public LocalDate getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(LocalDate updateTime) {
        this.updateTime = updateTime;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public Long getHospitalId() {
        return hospitalId;
    }

    public void setHospitalId(Long hospitalId) {
        this.hospitalId = hospitalId;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public String getDoctor() {
        return doctor;
    }

    public void setDoctor(String doctor) {
        this.doctor = doctor;
    }

    public LocalDate getStopTime() {
        return stopTime;
    }

    public void setStopTime(LocalDate stopTime) {
        this.stopTime = stopTime;
    }

    public LocalDate getStartTime() {
        return startTime;
    }

    public void setStartTime(LocalDate startTime) {
        this.startTime = startTime;
    }

    public LocalDate getCreateTime() {
        return createTime;
    }

    public void setCreateTime(LocalDate createTime) {
        this.createTime = createTime;
    }

    @Override
    public String toString() {
        return "Appointment{" +
                "appointmentId='" + appointmentId + '\'' +
                ", hospitalId=" + hospitalId +
                ", department='" + department + '\'' +
                ", doctor='" + doctor + '\'' +
                ", stopTime=" + stopTime +
                ", startTime=" + startTime +
                ", createTime=" + createTime +
                '}';
    }
}
