package com.scau.phr.domain.entity;

import java.time.LocalDateTime;

import com.baomidou.mybatisplus.annotation.*;
import com.baomidou.mybatisplus.annotation.TableField;

import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author yuzhiyi
 * @since 2018-08-05
 */
public class TreatmentRecoed implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "treatment_id", type = IdType.NONE)
    private Long treatmentId;
    @TableField("case_id")
    private Long caseId;
    @TableField("serviceprovider_id")
    private Long serviceproviderId;
    @TableField("treatment_content")
    private String treatmentContent;
    @TableField("treatment_result")
    private String treatmentResult;
    @TableField("recipe_id")
    private Long recipeId;
    @TableField("remark")
    private String remark;
    @TableField("cost")
    private Long cost;
    @TableField("is_treat_remind")
    private Boolean treatRemind;
    @TableField("remind_time")
    private LocalDateTime remindTime;
    @TableField(value = "create_time", fill = FieldFill.INSERT)
    private LocalDateTime createTime;
    @TableField(value = "update_time", fill = FieldFill.INSERT_UPDATE)
    private LocalDateTime updateTime;


    public Long getTreatmentId() {
        return treatmentId;
    }

    public void setTreatmentId(Long treatmentId) {
        this.treatmentId = treatmentId;
    }

    public Long getCaseId() {
        return caseId;
    }

    public void setCaseId(Long caseId) {
        this.caseId = caseId;
    }

    public Long getServiceproviderId() {
        return serviceproviderId;
    }

    public void setServiceproviderId(Long serviceproviderId) {
        this.serviceproviderId = serviceproviderId;
    }

    public String getTreatmentContent() {
        return treatmentContent;
    }

    public void setTreatmentContent(String treatmentContent) {
        this.treatmentContent = treatmentContent;
    }

    public String getTreatmentResult() {
        return treatmentResult;
    }

    public void setTreatmentResult(String treatmentResult) {
        this.treatmentResult = treatmentResult;
    }

    public Long getRecipeId() {
        return recipeId;
    }

    public void setRecipeId(Long recipeId) {
        this.recipeId = recipeId;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public Long getCost() {
        return cost;
    }

    public void setCost(Long cost) {
        this.cost = cost;
    }

    public Boolean getTreatRemind() {
        return treatRemind;
    }

    public void setTreatRemind(Boolean treatRemind) {
        this.treatRemind = treatRemind;
    }

    public LocalDateTime getRemindTime() {
        return remindTime;
    }

    public void setRemindTime(LocalDateTime remindTime) {
        this.remindTime = remindTime;
    }

    public LocalDateTime getCreateTime() {
        return createTime;
    }

    public void setCreateTime(LocalDateTime createTime) {
        this.createTime = createTime;
    }

    public LocalDateTime getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(LocalDateTime updateTime) {
        this.updateTime = updateTime;
    }

    @Override
    public String toString() {
        return "TreatmentRecoed{" +
        ", treatmentId=" + treatmentId +
        ", caseId=" + caseId +
        ", serviceproviderId=" + serviceproviderId +
        ", treatmentContent=" + treatmentContent +
        ", treatmentResult=" + treatmentResult +
        ", recipeId=" + recipeId +
        ", remark=" + remark +
        ", cost=" + cost +
        ", treatRemind=" + treatRemind +
        ", remindTime=" + remindTime +
        ", createTime=" + createTime +
        ", updateTime=" + updateTime +
        "}";
    }
}
