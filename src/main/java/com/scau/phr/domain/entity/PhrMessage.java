package com.scau.phr.domain.entity;

import java.time.LocalDate;
import java.time.LocalDateTime;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.IdType;
import java.io.Serializable;

/**
 * <p>
 * 站内信息
 * </p>
 *
 * @author yuzhiyi
 * @since 2018-08-13
 */
public class PhrMessage implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;
    /**
     * 发送者
     */
    @TableField("sender")
    private String sender;
    /**
     * 接受者手机号
     */
    @TableField("receiver")
    private String receiver;
    @TableField("content")
    private String content;
    @TableField("send_time")
    private LocalDateTime sendTime;
    /**
     * 消息类型
     */
    @TableField("message_type")
    private String messageType;
    /**
     * 是否已读
     */
    @TableField("readed")
    private Boolean readed;
    @TableField(value = "create_time", fill = FieldFill.INSERT)
    private LocalDate createTime;
    @TableField(value = "update_time", fill = FieldFill.INSERT_UPDATE)
    private LocalDate updateTime;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getSender() {
        return sender;
    }

    public void setSender(String sender) {
        this.sender = sender;
    }

    public String getReceiver() {
        return receiver;
    }

    public void setReceiver(String receiver) {
        this.receiver = receiver;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public LocalDateTime getSendTime() {
        return sendTime;
    }

    public void setSendTime(LocalDateTime sendTime) {
        this.sendTime = sendTime;
    }

    public Boolean getReaded() {
        return readed;
    }

    public void setReaded(Boolean readed) {
        this.readed = readed;
    }

    public LocalDate getCreateTime() {
        return createTime;
    }

    public void setCreateTime(LocalDate createTime) {
        this.createTime = createTime;
    }

    public LocalDate getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(LocalDate updateTime) {
        this.updateTime = updateTime;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public String getMessageType() {
        return messageType;
    }

    public void setMessageType(String messageType) {
        this.messageType = messageType;
    }

    @Override
    public String toString() {
        return "PhrMessage{" +
        ", id=" + id +
        ", sender=" + sender +
        ", receiver=" + receiver +
        ", content=" + content +
        ", sendTime=" + sendTime +
        ", readed=" + readed +
        ", createTime=" + createTime +
        ", updateTime=" + updateTime +
        "}";
    }
}
