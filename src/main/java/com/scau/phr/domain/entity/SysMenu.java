package com.scau.phr.domain.entity;

import java.time.LocalDateTime;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.FieldFill;;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.IdType;
import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author yuzhiyi
 * @since 2018-08-05
 */
public class SysMenu implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableField("mentality_id")
    private Long mentalityId;
    @TableField("physical_id")
    private Long physicalId;
    @TableField("healthcare_id")
    private Long healthcareId;
    @TableField("immune_id")
    private Long immuneId;
    @TableField(value = "create_time", fill = FieldFill.INSERT)
    private LocalDateTime createTime;
    @TableField(value = "update_time", fill = FieldFill.INSERT_UPDATE)
    private LocalDateTime updateTime;


    public Long getMentalityId() {
        return mentalityId;
    }

    public void setMentalityId(Long mentalityId) {
        this.mentalityId = mentalityId;
    }

    public Long getPhysicalId() {
        return physicalId;
    }

    public void setPhysicalId(Long physicalId) {
        this.physicalId = physicalId;
    }

    public Long getHealthcareId() {
        return healthcareId;
    }

    public void setHealthcareId(Long healthcareId) {
        this.healthcareId = healthcareId;
    }

    public Long getImmuneId() {
        return immuneId;
    }

    public void setImmuneId(Long immuneId) {
        this.immuneId = immuneId;
    }

    public LocalDateTime getCreateTime() {
        return createTime;
    }

    public void setCreateTime(LocalDateTime createTime) {
        this.createTime = createTime;
    }

    public LocalDateTime getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(LocalDateTime updateTime) {
        this.updateTime = updateTime;
    }

    @Override
    public String toString() {
        return "SysMenu{" +
        ", mentalityId=" + mentalityId +
        ", physicalId=" + physicalId +
        ", healthcareId=" + healthcareId +
        ", immuneId=" + immuneId +
        ", createTime=" + createTime +
        ", updateTime=" + updateTime +
        "}";
    }
}
