package com.scau.phr.domain.entity;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.FieldFill;;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.IdType;
import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author yuzhiyi
 * @since 2018-08-05
 */
public class RecentState implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "recent_state_id", type = IdType.NONE)
    private Long recentStateId;
    @TableField("health_record_id")
    private String healthRecordId;
    @TableField("record_id")
    private String recordId;
    @TableField("user_name")
    private String userName;
    @TableField("temperature")
    private String temperature;
    @TableField("pulse_rate")
    private String pulseRate;
    @TableField("respiratory_rate")
    private String respiratoryRate;
    @TableField("waistline")
    private BigDecimal waistline;
    @TableField("constitutionalindex")
    private BigDecimal constitutionalindex;
    @TableField("mouth")
    private String mouth;
    @TableField("vision")
    private String vision;
    @TableField("hearing")
    private String hearing;
    @TableField("sporting_abilities")
    private String sportingAbilities;
    @TableField("eyeground")
    private String eyeground;
    @TableField("skin")
    private String skin;
    @TableField("lymph_gland")
    private String lymphGland;
    @TableField("lung")
    private String lung;
    @TableField("heart")
    private String heart;
    @TableField("abdomen")
    private String abdomen;
    @TableField(value = "create_time", fill = FieldFill.INSERT)
    private LocalDateTime createTime;
    @TableField(value = "update_time", fill = FieldFill.INSERT_UPDATE)
    private LocalDateTime updateTime;


    public Long getRecentStateId() {
        return recentStateId;
    }

    public void setRecentStateId(Long recentStateId) {
        this.recentStateId = recentStateId;
    }

    public String getHealthRecordId() {
        return healthRecordId;
    }

    public void setHealthRecordId(String healthRecordId) {
        this.healthRecordId = healthRecordId;
    }

    public String getRecordId() {
        return recordId;
    }

    public void setRecordId(String recordId) {
        this.recordId = recordId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getTemperature() {
        return temperature;
    }

    public void setTemperature(String temperature) {
        this.temperature = temperature;
    }

    public String getPulseRate() {
        return pulseRate;
    }

    public void setPulseRate(String pulseRate) {
        this.pulseRate = pulseRate;
    }

    public String getRespiratoryRate() {
        return respiratoryRate;
    }

    public void setRespiratoryRate(String respiratoryRate) {
        this.respiratoryRate = respiratoryRate;
    }

    public BigDecimal getWaistline() {
        return waistline;
    }

    public void setWaistline(BigDecimal waistline) {
        this.waistline = waistline;
    }

    public BigDecimal getConstitutionalindex() {
        return constitutionalindex;
    }

    public void setConstitutionalindex(BigDecimal constitutionalindex) {
        this.constitutionalindex = constitutionalindex;
    }

    public String getMouth() {
        return mouth;
    }

    public void setMouth(String mouth) {
        this.mouth = mouth;
    }

    public String getVision() {
        return vision;
    }

    public void setVision(String vision) {
        this.vision = vision;
    }

    public String getHearing() {
        return hearing;
    }

    public void setHearing(String hearing) {
        this.hearing = hearing;
    }

    public String getSportingAbilities() {
        return sportingAbilities;
    }

    public void setSportingAbilities(String sportingAbilities) {
        this.sportingAbilities = sportingAbilities;
    }

    public String getEyeground() {
        return eyeground;
    }

    public void setEyeground(String eyeground) {
        this.eyeground = eyeground;
    }

    public String getSkin() {
        return skin;
    }

    public void setSkin(String skin) {
        this.skin = skin;
    }

    public String getLymphGland() {
        return lymphGland;
    }

    public void setLymphGland(String lymphGland) {
        this.lymphGland = lymphGland;
    }

    public String getLung() {
        return lung;
    }

    public void setLung(String lung) {
        this.lung = lung;
    }

    public String getHeart() {
        return heart;
    }

    public void setHeart(String heart) {
        this.heart = heart;
    }

    public String getAbdomen() {
        return abdomen;
    }

    public void setAbdomen(String abdomen) {
        this.abdomen = abdomen;
    }

    public LocalDateTime getCreateTime() {
        return createTime;
    }

    public void setCreateTime(LocalDateTime createTime) {
        this.createTime = createTime;
    }

    public LocalDateTime getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(LocalDateTime updateTime) {
        this.updateTime = updateTime;
    }

    @Override
    public String toString() {
        return "RecentState{" +
        ", recentStateId=" + recentStateId +
        ", healthRecordId=" + healthRecordId +
        ", recordId=" + recordId +
        ", userName=" + userName +
        ", temperature=" + temperature +
        ", pulseRate=" + pulseRate +
        ", respiratoryRate=" + respiratoryRate +
        ", waistline=" + waistline +
        ", constitutionalindex=" + constitutionalindex +
        ", mouth=" + mouth +
        ", vision=" + vision +
        ", hearing=" + hearing +
        ", sportingAbilities=" + sportingAbilities +
        ", eyeground=" + eyeground +
        ", skin=" + skin +
        ", lymphGland=" + lymphGland +
        ", lung=" + lung +
        ", heart=" + heart +
        ", abdomen=" + abdomen +
        ", createTime=" + createTime +
        ", updateTime=" + updateTime +
        "}";
    }
}
