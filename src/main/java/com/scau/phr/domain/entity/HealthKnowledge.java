package com.scau.phr.domain.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.FieldFill;;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.IdType;


import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author yuzhiyi
 * @since 2018-08-05
 */
public class HealthKnowledge implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;
    @TableField("title")
    private String title;
    @TableField("type")
    private String type;
    @TableField("author")
    private String author;
    @TableField("image")
    private String image;
    /**
     * 新闻发布时间
     */
    @TableField("release_time")
    private LocalDateTime releaseTime;
    @TableField("content")
    private String content;
    @TableField("visits")
    private Integer visits;
    @TableField(value = "create_time", fill = FieldFill.INSERT)
    private LocalDateTime createTime;
    @TableField(value = "update_time", fill = FieldFill.INSERT_UPDATE)
    private LocalDateTime updateTime;
    /**
     * 爬取的网站
     */
    @TableField("source_url")
    private String sourceUrl;
    /**
     * 简介
     */
    @TableField("description")
    private String description;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public LocalDateTime getReleaseTime() {
        return releaseTime;
    }

    public void setReleaseTime(LocalDateTime releaseTime) {
        this.releaseTime = releaseTime;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Integer getVisits() {
        return visits;
    }

    public void setVisits(Integer visits) {
        this.visits = visits;
    }

    public LocalDateTime getCreateTime() {
        return createTime;
    }

    public void setCreateTime(LocalDateTime createTime) {
        this.createTime = createTime;
    }

    public LocalDateTime getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(LocalDateTime updateTime) {
        this.updateTime = updateTime;
    }

    public String getSourceUrl() {
        return sourceUrl;
    }

    public void setSourceUrl(String sourceUrl) {
        this.sourceUrl = sourceUrl;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    @Override
    public String toString() {
        return "HealthKnowledge{" +
        ", id=" + id +
        ", title=" + title +
        ", type=" + type +
        ", author=" + author +
        ", releaseTime=" + releaseTime +
        ", content=" + content +
        ", visits=" + visits +
        ", createTime=" + createTime +
        ", updateTime=" + updateTime +
        ", sourceUrl=" + sourceUrl +
        ", description=" + description +
        "}";
    }
}
