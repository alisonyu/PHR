package com.scau.phr.domain.entity;

import java.time.LocalDateTime;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.FieldFill;;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.IdType;
import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author yuzhiyi
 * @since 2018-08-05
 */
public class PhysicalExamPackage implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "physical_exam_id", type = IdType.NONE)
    private Long physicalExamId;
    @TableField("exam_package_id")
    private Long examPackageId;
    @TableField("package_detail")
    private String packageDetail;
    @TableField("limit_person")
    private String limitPerson;
    @TableField("matters")
    private String matters;
    @TableField("stop_time")
    private LocalDateTime stopTime;
    @TableField("start_time")
    private LocalDateTime startTime;
    @TableField(value = "create_time", fill = FieldFill.INSERT)
    private LocalDateTime createTime;
    @TableField("provider_id")
    private String providerId;


    public Long getPhysicalExamId() {
        return physicalExamId;
    }

    public void setPhysicalExamId(Long physicalExamId) {
        this.physicalExamId = physicalExamId;
    }

    public Long getExamPackageId() {
        return examPackageId;
    }

    public void setExamPackageId(Long examPackageId) {
        this.examPackageId = examPackageId;
    }

    public String getPackageDetail() {
        return packageDetail;
    }

    public void setPackageDetail(String packageDetail) {
        this.packageDetail = packageDetail;
    }

    public String getLimitPerson() {
        return limitPerson;
    }

    public void setLimitPerson(String limitPerson) {
        this.limitPerson = limitPerson;
    }

    public String getMatters() {
        return matters;
    }

    public void setMatters(String matters) {
        this.matters = matters;
    }

    public LocalDateTime getStopTime() {
        return stopTime;
    }

    public void setStopTime(LocalDateTime stopTime) {
        this.stopTime = stopTime;
    }

    public LocalDateTime getStartTime() {
        return startTime;
    }

    public void setStartTime(LocalDateTime startTime) {
        this.startTime = startTime;
    }

    public LocalDateTime getCreateTime() {
        return createTime;
    }

    public void setCreateTime(LocalDateTime createTime) {
        this.createTime = createTime;
    }

    public String getProviderId() {
        return providerId;
    }

    public void setProviderId(String providerId) {
        this.providerId = providerId;
    }

    @Override
    public String toString() {
        return "PhysicalExamPackage{" +
        ", physicalExamId=" + physicalExamId +
        ", examPackageId=" + examPackageId +
        ", packageDetail=" + packageDetail +
        ", limitPerson=" + limitPerson +
        ", matters=" + matters +
        ", stopTime=" + stopTime +
        ", startTime=" + startTime +
        ", createTime=" + createTime +
        ", providerId=" + providerId +
        "}";
    }
}
