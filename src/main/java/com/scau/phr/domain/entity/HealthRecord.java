package com.scau.phr.domain.entity;

import java.time.LocalDate;
import java.time.LocalDateTime;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.FieldFill;;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.IdType;
import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author yuzhiyi
 * @since 2018-08-05
 */
public class HealthRecord implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "health_record_id", type = IdType.AUTO)
    private Long healthRecordId;
    @TableField("telephone")
    private String telephone;
    @TableField("sel_registration_id")
    private Long selRegistrationId;
    @TableField("fur_return_id")
    private String furReturnId;
    @TableField("user_id")
    private Long userId;
    @TableField("record_buildorganization")
    private String recordBuildorganization;
    @TableField("doctor_id")
    private String doctorId;
    @TableField("record_createdate")
    private LocalDate recordCreatedate;
    @TableField("occupation")
    private String occupation;
    @TableField("marital_status")
    private String maritalStatus;
    @TableField("payment_method")
    private String paymentMethod;
    @TableField("drugallergy_history")
    private String drugallergyHistory;
    @TableField("expousre_history")
    private String expousreHistory;
    @TableField("disease_history")
    private String diseaseHistory;
    @TableField("disability")
    private String disability;
    @TableField("symptom")
    private String symptom;
    @TableField("exercise")
    private String exercise;
    @TableField("eating_habit")
    private String eatingHabit;
    @TableField("smoking")
    private String smoking;
    @TableField("drinking")
    private String drinking;
    @TableField("living_environment")
    private String livingEnvironment;
    @TableField("physicalexam_id")
    private Long physicalexamId;
    @TableField("clinic_id")
    private Long clinicId;
    @TableField("registration_id")
    private Long registrationId;
    @TableField("return_id")
    private Long returnId;
    @TableField("recent_state_id")
    private Long recentStateId;
    @TableField(value = "create_time", fill = FieldFill.INSERT)
    private LocalDate createTime;
    @TableField(value = "update_time", fill = FieldFill.INSERT_UPDATE)
    private LocalDate updateTime;


    public Long getHealthRecordId() {
        return healthRecordId;
    }

    public void setHealthRecordId(Long healthRecordId) {
        this.healthRecordId = healthRecordId;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public Long getSelRegistrationId() {
        return selRegistrationId;
    }

    public void setSelRegistrationId(Long selRegistrationId) {
        this.selRegistrationId = selRegistrationId;
    }

    public String getFurReturnId() {
        return furReturnId;
    }

    public void setFurReturnId(String furReturnId) {
        this.furReturnId = furReturnId;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getRecordBuildorganization() {
        return recordBuildorganization;
    }

    public void setRecordBuildorganization(String recordBuildorganization) {
        this.recordBuildorganization = recordBuildorganization;
    }

    public String getDoctorId() {
        return doctorId;
    }

    public void setDoctorId(String doctorId) {
        this.doctorId = doctorId;
    }

    public LocalDate getRecordCreatedate() {
        return recordCreatedate;
    }

    public void setRecordCreatedate(LocalDate recordCreatedate) {
        this.recordCreatedate = recordCreatedate;
    }

    public String getOccupation() {
        return occupation;
    }

    public void setOccupation(String occupation) {
        this.occupation = occupation;
    }

    public String getMaritalStatus() {
        return maritalStatus;
    }

    public void setMaritalStatus(String maritalStatus) {
        this.maritalStatus = maritalStatus;
    }

    public String getPaymentMethod() {
        return paymentMethod;
    }

    public void setPaymentMethod(String paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    public String getDrugallergyHistory() {
        return drugallergyHistory;
    }

    public void setDrugallergyHistory(String drugallergyHistory) {
        this.drugallergyHistory = drugallergyHistory;
    }

    public String getExpousreHistory() {
        return expousreHistory;
    }

    public void setExpousreHistory(String expousreHistory) {
        this.expousreHistory = expousreHistory;
    }

    public String getDiseaseHistory() {
        return diseaseHistory;
    }

    public void setDiseaseHistory(String diseaseHistory) {
        this.diseaseHistory = diseaseHistory;
    }

    public String getDisability() {
        return disability;
    }

    public void setDisability(String disability) {
        this.disability = disability;
    }

    public String getSymptom() {
        return symptom;
    }

    public void setSymptom(String symptom) {
        this.symptom = symptom;
    }

    public String getExercise() {
        return exercise;
    }

    public void setExercise(String exercise) {
        this.exercise = exercise;
    }

    public String getEatingHabit() {
        return eatingHabit;
    }

    public void setEatingHabit(String eatingHabit) {
        this.eatingHabit = eatingHabit;
    }

    public String getSmoking() {
        return smoking;
    }

    public void setSmoking(String smoking) {
        this.smoking = smoking;
    }

    public String getDrinking() {
        return drinking;
    }

    public void setDrinking(String drinking) {
        this.drinking = drinking;
    }

    public String getLivingEnvironment() {
        return livingEnvironment;
    }

    public void setLivingEnvironment(String livingEnvironment) {
        this.livingEnvironment = livingEnvironment;
    }

    public Long getPhysicalexamId() {
        return physicalexamId;
    }

    public void setPhysicalexamId(Long physicalexamId) {
        this.physicalexamId = physicalexamId;
    }

    public Long getClinicId() {
        return clinicId;
    }

    public void setClinicId(Long clinicId) {
        this.clinicId = clinicId;
    }

    public Long getRegistrationId() {
        return registrationId;
    }

    public void setRegistrationId(Long registrationId) {
        this.registrationId = registrationId;
    }

    public Long getReturnId() {
        return returnId;
    }

    public void setReturnId(Long returnId) {
        this.returnId = returnId;
    }

    public Long getRecentStateId() {
        return recentStateId;
    }

    public void setRecentStateId(Long recentStateId) {
        this.recentStateId = recentStateId;
    }

    public LocalDate getCreateTime() {
        return createTime;
    }

    public void setCreateTime(LocalDate createTime) {
        this.createTime = createTime;
    }

    public LocalDate getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(LocalDate updateTime) {
        this.updateTime = updateTime;
    }

    @Override
    public String toString() {
        return "HealthRecord{" +
        ", healthRecordId=" + healthRecordId +
        ", telephone=" + telephone +
        ", selRegistrationId=" + selRegistrationId +
        ", furReturnId=" + furReturnId +
        ", userId=" + userId +
        ", recordBuildorganization=" + recordBuildorganization +
        ", doctorId=" + doctorId +
        ", recordCreatedate=" + recordCreatedate +
        ", occupation=" + occupation +
        ", maritalStatus=" + maritalStatus +
        ", paymentMethod=" + paymentMethod +
        ", drugallergyHistory=" + drugallergyHistory +
        ", expousreHistory=" + expousreHistory +
        ", diseaseHistory=" + diseaseHistory +
        ", disability=" + disability +
        ", symptom=" + symptom +
        ", exercise=" + exercise +
        ", eatingHabit=" + eatingHabit +
        ", smoking=" + smoking +
        ", drinking=" + drinking +
        ", livingEnvironment=" + livingEnvironment +
        ", physicalexamId=" + physicalexamId +
        ", clinicId=" + clinicId +
        ", registrationId=" + registrationId +
        ", returnId=" + returnId +
        ", recentStateId=" + recentStateId +
        ", createTime=" + createTime +
        ", updateTime=" + updateTime +
        "}";
    }
}
