package com.scau.phr.domain.entity;

import java.time.LocalDateTime;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.FieldFill;;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.IdType;
import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author yuzhiyi
 * @since 2018-08-05
 */
public class PhysicalExam implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "physical_exam_id", type = IdType.NONE)
    private Long physicalExamId;
    @TableField("phy_physical_exam_id")
    private Long phyPhysicalExamId;
    @TableField("health_record_id")
    private String healthRecordId;
    @TableField("record_id")
    private Long recordId;
    @TableField("exam_content")
    private String examContent;
    @TableField("exam_result")
    private String examResult;
    @TableField("exam_doctor")
    private String examDoctor;
    @TableField("provider_id")
    private Long providerId;
    @TableField("appointment_id")
    private Long appointmentId;
    @TableField("exampackage_id")
    private Long exampackageId;
    @TableField(value = "create_time", fill = FieldFill.INSERT)
    private LocalDateTime createTime;
    @TableField(value = "update_time", fill = FieldFill.INSERT_UPDATE)
    private LocalDateTime updateTime;


    public Long getPhysicalExamId() {
        return physicalExamId;
    }

    public void setPhysicalExamId(Long physicalExamId) {
        this.physicalExamId = physicalExamId;
    }

    public Long getPhyPhysicalExamId() {
        return phyPhysicalExamId;
    }

    public void setPhyPhysicalExamId(Long phyPhysicalExamId) {
        this.phyPhysicalExamId = phyPhysicalExamId;
    }

    public String getHealthRecordId() {
        return healthRecordId;
    }

    public void setHealthRecordId(String healthRecordId) {
        this.healthRecordId = healthRecordId;
    }

    public Long getRecordId() {
        return recordId;
    }

    public void setRecordId(Long recordId) {
        this.recordId = recordId;
    }

    public String getExamContent() {
        return examContent;
    }

    public void setExamContent(String examContent) {
        this.examContent = examContent;
    }

    public String getExamResult() {
        return examResult;
    }

    public void setExamResult(String examResult) {
        this.examResult = examResult;
    }

    public String getExamDoctor() {
        return examDoctor;
    }

    public void setExamDoctor(String examDoctor) {
        this.examDoctor = examDoctor;
    }

    public Long getProviderId() {
        return providerId;
    }

    public void setProviderId(Long providerId) {
        this.providerId = providerId;
    }

    public Long getAppointmentId() {
        return appointmentId;
    }

    public void setAppointmentId(Long appointmentId) {
        this.appointmentId = appointmentId;
    }

    public Long getExampackageId() {
        return exampackageId;
    }

    public void setExampackageId(Long exampackageId) {
        this.exampackageId = exampackageId;
    }

    public LocalDateTime getCreateTime() {
        return createTime;
    }

    public void setCreateTime(LocalDateTime createTime) {
        this.createTime = createTime;
    }

    public LocalDateTime getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(LocalDateTime updateTime) {
        this.updateTime = updateTime;
    }

    @Override
    public String toString() {
        return "PhysicalExam{" +
        ", physicalExamId=" + physicalExamId +
        ", phyPhysicalExamId=" + phyPhysicalExamId +
        ", healthRecordId=" + healthRecordId +
        ", recordId=" + recordId +
        ", examContent=" + examContent +
        ", examResult=" + examResult +
        ", examDoctor=" + examDoctor +
        ", providerId=" + providerId +
        ", appointmentId=" + appointmentId +
        ", exampackageId=" + exampackageId +
        ", createTime=" + createTime +
        ", updateTime=" + updateTime +
        "}";
    }
}
