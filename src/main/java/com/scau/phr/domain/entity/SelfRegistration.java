package com.scau.phr.domain.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.IdType;
import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author yuzhiyi
 * @since 2018-08-05
 */
public class SelfRegistration implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "registration_id", type = IdType.NONE)
    private Long registrationId;
    @TableField("hea_healthrecord_id")
    private String heaHealthrecordId;
    @TableField("healthrecord_id")
    private Long healthrecordId;
    @TableField("register_id")
    private Long registerId;


    public Long getRegistrationId() {
        return registrationId;
    }

    public void setRegistrationId(Long registrationId) {
        this.registrationId = registrationId;
    }

    public String getHeaHealthrecordId() {
        return heaHealthrecordId;
    }

    public void setHeaHealthrecordId(String heaHealthrecordId) {
        this.heaHealthrecordId = heaHealthrecordId;
    }

    public Long getHealthrecordId() {
        return healthrecordId;
    }

    public void setHealthrecordId(Long healthrecordId) {
        this.healthrecordId = healthrecordId;
    }

    public Long getRegisterId() {
        return registerId;
    }

    public void setRegisterId(Long registerId) {
        this.registerId = registerId;
    }

    @Override
    public String toString() {
        return "SelfRegistration{" +
        ", registrationId=" + registrationId +
        ", heaHealthrecordId=" + heaHealthrecordId +
        ", healthrecordId=" + healthrecordId +
        ", registerId=" + registerId +
        "}";
    }
}
