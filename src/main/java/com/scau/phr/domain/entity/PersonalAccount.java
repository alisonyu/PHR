package com.scau.phr.domain.entity;

import java.time.LocalDateTime;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.FieldFill;;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.IdType;
import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author yuzhiyi
 * @since 2018-08-05
 */
public class PersonalAccount implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "personal_account_id", type = IdType.NONE)
    private Long personalAccountId;
    @TableField("user_id")
    private Long userId;
    @TableField("login_name")
    private String loginName;
    @TableField("password")
    private String password;
    @TableField("is_protectpsw")
    private Boolean protectpsw;
    @TableField("real_name")
    private String realName;
    @TableField("idcard_num")
    private String idcardNum;
    @TableField("is_verify")
    private Boolean verify;
    @TableField("is_vip")
    private Boolean vip;
    @TableField("payment_id")
    private Long paymentId;
    @TableField("method_payment")
    private Integer methodPayment;
    @TableField(value = "create_time", fill = FieldFill.INSERT)
    private LocalDateTime createTime;
    @TableField(value = "update_time", fill = FieldFill.INSERT_UPDATE)
    private LocalDateTime updateTime;


    public Long getPersonalAccountId() {
        return personalAccountId;
    }

    public void setPersonalAccountId(Long personalAccountId) {
        this.personalAccountId = personalAccountId;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getLoginName() {
        return loginName;
    }

    public void setLoginName(String loginName) {
        this.loginName = loginName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Boolean getProtectpsw() {
        return protectpsw;
    }

    public void setProtectpsw(Boolean protectpsw) {
        this.protectpsw = protectpsw;
    }

    public String getRealName() {
        return realName;
    }

    public void setRealName(String realName) {
        this.realName = realName;
    }

    public String getIdcardNum() {
        return idcardNum;
    }

    public void setIdcardNum(String idcardNum) {
        this.idcardNum = idcardNum;
    }

    public Boolean getVerify() {
        return verify;
    }

    public void setVerify(Boolean verify) {
        this.verify = verify;
    }

    public Boolean getVip() {
        return vip;
    }

    public void setVip(Boolean vip) {
        this.vip = vip;
    }

    public Long getPaymentId() {
        return paymentId;
    }

    public void setPaymentId(Long paymentId) {
        this.paymentId = paymentId;
    }

    public Integer getMethodPayment() {
        return methodPayment;
    }

    public void setMethodPayment(Integer methodPayment) {
        this.methodPayment = methodPayment;
    }

    public LocalDateTime getCreateTime() {
        return createTime;
    }

    public void setCreateTime(LocalDateTime createTime) {
        this.createTime = createTime;
    }

    public LocalDateTime getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(LocalDateTime updateTime) {
        this.updateTime = updateTime;
    }

    @Override
    public String toString() {
        return "PersonalAccount{" +
        ", personalAccountId=" + personalAccountId +
        ", userId=" + userId +
        ", loginName=" + loginName +
        ", password=" + password +
        ", protectpsw=" + protectpsw +
        ", realName=" + realName +
        ", idcardNum=" + idcardNum +
        ", verify=" + verify +
        ", vip=" + vip +
        ", paymentId=" + paymentId +
        ", methodPayment=" + methodPayment +
        ", createTime=" + createTime +
        ", updateTime=" + updateTime +
        "}";
    }
}
