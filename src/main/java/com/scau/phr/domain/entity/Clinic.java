package com.scau.phr.domain.entity;

import java.time.LocalDateTime;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.FieldFill;;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.IdType;
import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author yuzhiyi
 * @since 2018-08-05
 */
public class Clinic implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "clinic_id", type = IdType.NONE)
    private Long clinicId;
    @TableField("health_record_id")
    private String healthRecordId;
    @TableField("record_id")
    private String recordId;
    @TableField("clinic_content")
    private String clinicContent;
    @TableField("clinic_result")
    private String clinicResult;
    @TableField("drug_id")
    private Long drugId;
    @TableField("remark")
    private String remark;
    @TableField("doctor_name")
    private String doctorName;
    @TableField("provider_id")
    private Long providerId;
    @TableField(value = "create_time", fill = FieldFill.INSERT)
    private LocalDateTime createTime;
    @TableField(value = "update_time", fill = FieldFill.INSERT_UPDATE)
    private LocalDateTime updateTime;
    @TableField("return_id")
    private Long returnId;


    public Long getClinicId() {
        return clinicId;
    }

    public void setClinicId(Long clinicId) {
        this.clinicId = clinicId;
    }

    public String getHealthRecordId() {
        return healthRecordId;
    }

    public void setHealthRecordId(String healthRecordId) {
        this.healthRecordId = healthRecordId;
    }

    public String getRecordId() {
        return recordId;
    }

    public void setRecordId(String recordId) {
        this.recordId = recordId;
    }

    public String getClinicContent() {
        return clinicContent;
    }

    public void setClinicContent(String clinicContent) {
        this.clinicContent = clinicContent;
    }

    public String getClinicResult() {
        return clinicResult;
    }

    public void setClinicResult(String clinicResult) {
        this.clinicResult = clinicResult;
    }

    public Long getDrugId() {
        return drugId;
    }

    public void setDrugId(Long drugId) {
        this.drugId = drugId;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getDoctorName() {
        return doctorName;
    }

    public void setDoctorName(String doctorName) {
        this.doctorName = doctorName;
    }

    public Long getProviderId() {
        return providerId;
    }

    public void setProviderId(Long providerId) {
        this.providerId = providerId;
    }

    public LocalDateTime getCreateTime() {
        return createTime;
    }

    public void setCreateTime(LocalDateTime createTime) {
        this.createTime = createTime;
    }

    public LocalDateTime getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(LocalDateTime updateTime) {
        this.updateTime = updateTime;
    }

    public Long getReturnId() {
        return returnId;
    }

    public void setReturnId(Long returnId) {
        this.returnId = returnId;
    }

    @Override
    public String toString() {
        return "Clinic{" +
        ", clinicId=" + clinicId +
        ", healthRecordId=" + healthRecordId +
        ", recordId=" + recordId +
        ", clinicContent=" + clinicContent +
        ", clinicResult=" + clinicResult +
        ", drugId=" + drugId +
        ", remark=" + remark +
        ", doctorName=" + doctorName +
        ", providerId=" + providerId +
        ", createTime=" + createTime +
        ", updateTime=" + updateTime +
        ", returnId=" + returnId +
        "}";
    }
}
