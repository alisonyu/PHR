package com.scau.phr.domain.entity;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.FieldFill;;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.IdType;
import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author yuzhiyi
 * @since 2018-08-05
 */
public class Payment implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "payment_id", type = IdType.NONE)
    private String paymentId;
    @TableField("per_personal_account_id")
    private Long perPersonalAccountId;
    @TableField("personal_account_id")
    private Long personalAccountId;
    @TableField("payment_account")
    private BigDecimal paymentAccount;
    @TableField(value = "create_time", fill = FieldFill.INSERT)
    private LocalDateTime createTime;
    @TableField(value = "update_time", fill = FieldFill.INSERT_UPDATE)
    private LocalDateTime updateTime;
    @TableField("payment_date")
    private LocalDate paymentDate;


    public String getPaymentId() {
        return paymentId;
    }

    public void setPaymentId(String paymentId) {
        this.paymentId = paymentId;
    }

    public Long getPerPersonalAccountId() {
        return perPersonalAccountId;
    }

    public void setPerPersonalAccountId(Long perPersonalAccountId) {
        this.perPersonalAccountId = perPersonalAccountId;
    }

    public Long getPersonalAccountId() {
        return personalAccountId;
    }

    public void setPersonalAccountId(Long personalAccountId) {
        this.personalAccountId = personalAccountId;
    }

    public BigDecimal getPaymentAccount() {
        return paymentAccount;
    }

    public void setPaymentAccount(BigDecimal paymentAccount) {
        this.paymentAccount = paymentAccount;
    }

    public LocalDateTime getCreateTime() {
        return createTime;
    }

    public void setCreateTime(LocalDateTime createTime) {
        this.createTime = createTime;
    }

    public LocalDateTime getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(LocalDateTime updateTime) {
        this.updateTime = updateTime;
    }

    public LocalDate getPaymentDate() {
        return paymentDate;
    }

    public void setPaymentDate(LocalDate paymentDate) {
        this.paymentDate = paymentDate;
    }

    @Override
    public String toString() {
        return "Payment{" +
        ", paymentId=" + paymentId +
        ", perPersonalAccountId=" + perPersonalAccountId +
        ", personalAccountId=" + personalAccountId +
        ", paymentAccount=" + paymentAccount +
        ", createTime=" + createTime +
        ", updateTime=" + updateTime +
        ", paymentDate=" + paymentDate +
        "}";
    }
}
