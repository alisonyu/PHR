package com.scau.phr.domain.entity;

import java.time.LocalDateTime;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.FieldFill;;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.IdType;
import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author yuzhiyi
 * @since 2018-08-05
 */
public class Registration implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "register_id", type = IdType.NONE)
    private String registerId;
    @TableField("registration_id")
    private Long registrationId;
    @TableField("regis_time")
    private LocalDateTime regisTime;
    @TableField("visit_time")
    private LocalDateTime visitTime;
    @TableField("registration_cost")
    private LocalDateTime registrationCost;
    @TableField(value = "create_time", fill = FieldFill.INSERT)
    private LocalDateTime createTime;
    @TableField(value = "update_time", fill = FieldFill.INSERT_UPDATE)
    private LocalDateTime updateTime;
    @TableField("remark")
    private String remark;
    @TableField("provider_id")
    private Long providerId;
    @TableField("department_id")
    private Long departmentId;
    @TableField("doctor_id")
    private Long doctorId;


    public String getRegisterId() {
        return registerId;
    }

    public void setRegisterId(String registerId) {
        this.registerId = registerId;
    }

    public Long getRegistrationId() {
        return registrationId;
    }

    public void setRegistrationId(Long registrationId) {
        this.registrationId = registrationId;
    }

    public LocalDateTime getRegisTime() {
        return regisTime;
    }

    public void setRegisTime(LocalDateTime regisTime) {
        this.regisTime = regisTime;
    }

    public LocalDateTime getVisitTime() {
        return visitTime;
    }

    public void setVisitTime(LocalDateTime visitTime) {
        this.visitTime = visitTime;
    }

    public LocalDateTime getRegistrationCost() {
        return registrationCost;
    }

    public void setRegistrationCost(LocalDateTime registrationCost) {
        this.registrationCost = registrationCost;
    }

    public LocalDateTime getCreateTime() {
        return createTime;
    }

    public void setCreateTime(LocalDateTime createTime) {
        this.createTime = createTime;
    }

    public LocalDateTime getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(LocalDateTime updateTime) {
        this.updateTime = updateTime;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public Long getProviderId() {
        return providerId;
    }

    public void setProviderId(Long providerId) {
        this.providerId = providerId;
    }

    public Long getDepartmentId() {
        return departmentId;
    }

    public void setDepartmentId(Long departmentId) {
        this.departmentId = departmentId;
    }

    public Long getDoctorId() {
        return doctorId;
    }

    public void setDoctorId(Long doctorId) {
        this.doctorId = doctorId;
    }

    @Override
    public String toString() {
        return "Registration{" +
        ", registerId=" + registerId +
        ", registrationId=" + registrationId +
        ", regisTime=" + regisTime +
        ", visitTime=" + visitTime +
        ", registrationCost=" + registrationCost +
        ", createTime=" + createTime +
        ", updateTime=" + updateTime +
        ", remark=" + remark +
        ", providerId=" + providerId +
        ", departmentId=" + departmentId +
        ", doctorId=" + doctorId +
        "}";
    }
}
