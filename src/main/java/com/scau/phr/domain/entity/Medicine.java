package com.scau.phr.domain.entity;

import com.baomidou.mybatisplus.annotation.TableField;

import javax.validation.constraints.NotNull;

/**
 * 医药信息类
 * 请使用MedicineData
 * @author yuzhiyi
 * @date 2018/7/13 16:03
 */
@Deprecated
public class Medicine {
    private Integer id;                 //药品ID
    @NotNull(message = "药名不能为空")
    private String name;                //药名
    private String alias;               //别名
    private String foreignName;         //外文名
    private String shape;               //性状
    private String pharmacology;        //药理毒理
    @NotNull(message = "作用特点不能为空")
    private String characteristics;     //作用特点
    private String dmpk;                //药物动力学
    private String facility;            //功能
    @NotNull(message = "主治不能为空")
    private String major;               //主治
    private String target;              //适应症
    @TableField("medicineUsage")
    private String medicineUsage;       //用法用量
    private String notice;              //注意事项
    @TableField("badReaction")
    private String badReaction;         //不良反应
    private String contraindication;    //禁忌症
    private String size;                //规格


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getForeignName() {
        return foreignName;
    }

    public void setForeignName(String foreignName) {
        this.foreignName = foreignName;
    }

    public String getTarget() {
        return target;
    }

    public void setTarget(String target) {
        this.target = target;
    }

    public String getMedicineUsage() {
        return medicineUsage;
    }

    public void setMedicineUsage(String medicianUsage) {
        this.medicineUsage = medicianUsage;
    }

    public String getNotice() {
        return notice;
    }

    public void setNotice(String notice) {
        this.notice = notice;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    public String getShape() {
        return shape;
    }

    public void setShape(String shape) {
        this.shape = shape;
    }

    public String getPharmacology() {
        return pharmacology;
    }

    public void setPharmacology(String pharmacology) {
        this.pharmacology = pharmacology;
    }

    public String getCharacteristics() {
        return characteristics;
    }

    public void setCharacteristics(String characteristics) {
        this.characteristics = characteristics;
    }

    public String getDmpk() {
        return dmpk;
    }

    public void setDmpk(String dmpk) {
        this.dmpk = dmpk;
    }

    public String getFacility() {
        return facility;
    }

    public void setFacility(String facility) {
        this.facility = facility;
    }

    public String getMajor() {
        return major;
    }

    public void setMajor(String major) {
        this.major = major;
    }

    @Override
    public String toString() {
        return "Medicine{" +
                "name='" + name + '\'' +
                ", alias='" + alias + '\'' +
                ", foreignName='" + foreignName + '\'' +
                ", shape='" + shape + '\'' +
                ", pharmacology='" + pharmacology + '\'' +
                ", characteristics='" + characteristics + '\'' +
                ", dmpk='" + dmpk + '\'' +
                ", facility='" + facility + '\'' +
                ", major='" + major + '\'' +
                ", target='" + target + '\'' +
                ", medicineUsage='" + medicineUsage + '\'' +
                ", notice='" + notice + '\'' +
                ", badReaction='" + badReaction + '\'' +
                ", contraindication='" + contraindication + '\'' +
                ", size='" + size + '\'' +
                '}';
    }

    public String getBadReaction() {
        return badReaction;
    }

    public void setBadReaction(String badReaction) {
        this.badReaction = badReaction;
    }

    public String getContraindication() {
        return contraindication;
    }

    public void setContraindication(String contraindication) {
        this.contraindication = contraindication;
    }

}

