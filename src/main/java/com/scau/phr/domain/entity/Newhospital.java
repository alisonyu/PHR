package com.scau.phr.domain.entity;

import com.baomidou.mybatisplus.annotation.*;
import com.baomidou.mybatisplus.annotation.TableId;
import org.apache.lucene.index.IndexableField;

import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author yuzhiyi
 * @since 2018-08-05
 */
public class Newhospital implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;
    @TableField("name")
    private String name;
    @TableField("phone_number")
    private String phoneNumber;
    @TableField("nature")
    private String nature;
    @TableField("hospital_type")
    private String hospitalType;
    @TableField("grade")
    private String grade;
    @TableField("address")
    private String address;
    @TableField("address_province")
    private String addressProvince;
    @TableField("address_city")
    private String addressCity;
    @TableField("address_county")
    private String addressCounty;
    @TableField("address_town")
    private String addressTown;
    @TableField("address_detail")
    private String addressDetail;
    @TableField("introduce")
    private String introduce;
    @TableField("longitude")
    private double longitude;
    @TableField("latitude")
    private double latitude;

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getNature() {
        return nature;
    }

    public void setNature(String nature) {
        this.nature = nature;
    }

    public String getHospitalType() {
        return hospitalType;
    }

    public void setHospitalType(String hospitalType) {
        this.hospitalType = hospitalType;
    }

    public String getGrade() {
        return grade;
    }

    public void setGrade(String grade) {
        this.grade = grade;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getAddressProvince() {
        return addressProvince;
    }

    public void setAddressProvince(String addressProvince) {
        this.addressProvince = addressProvince;
    }

    public String getAddressCity() {
        return addressCity;
    }

    public void setAddressCity(String addressCity) {
        this.addressCity = addressCity;
    }

    public String getAddressCounty() {
        return addressCounty;
    }

    public void setAddressCounty(String addressCounty) {
        this.addressCounty = addressCounty;
    }

    public String getAddressTown() {
        return addressTown;
    }

    public void setAddressTown(String addressTown) {
        this.addressTown = addressTown;
    }

    public String getAddressDetail() {
        return addressDetail;
    }

    public void setAddressDetail(String addressDetail) {
        this.addressDetail = addressDetail;
    }

    public String getIntroduce() {
        return introduce;
    }

    public void setIntroduce(String introduce) {
        this.introduce = introduce;
    }

    @Override
    public String toString() {
        return "Newhospital{" +
        ", id=" + id +
        ", name=" + name +
        ", phoneNumber=" + phoneNumber +
        ", nature=" + nature +
        ", hospitalType=" + hospitalType +
        ", grade=" + grade +
        ", address=" + address +
        ", addressProvince=" + addressProvince +
        ", addressCity=" + addressCity +
        ", addressCounty=" + addressCounty +
        ", addressTown=" + addressTown +
        ", addressDetail=" + addressDetail +
        ", introduce=" + introduce +
        "}";
    }
}
