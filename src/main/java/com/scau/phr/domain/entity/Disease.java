package com.scau.phr.domain.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author yuzhiyi
 * @since 2018-08-05
 */
@TableName("Disease")
public class Disease implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "name", type = IdType.NONE)
    private String name;
    @TableField("introduction")
    private String introduction;
    @TableField("is_insurance")
    private String isInsurance;
    @TableField("another_name")
    private String anotherName;
    @TableField("site_of_disease")
    private String siteOfDisease;
    @TableField("infectivity")
    private String infectivity;
    @TableField("type_of_infectivity")
    private String typeOfInfectivity;
    @TableField("transmission")
    private String transmission;
    @TableField("crowd")
    private String crowd;
    @TableField("symptoms")
    private String symptoms;
    @TableField("concurrent_disease")
    private String concurrentDisease;
    @TableField("incubation_period")
    private String incubationPeriod;
    @TableField("performance")
    private String performance;
    @TableField("Department")
    private String Department;
    @TableField("Cost")
    private String Cost;
    @TableField("cureRate")
    private String cureRate;
    @TableField("treatment_cycle")
    private String treatmentCycle;
    @TableField("treatment")
    private String treatment;
    @TableField("Correlation")
    private String Correlation;
    @TableField("medicinal")
    private String medicinal;
    @TableField("operation")
    private String operation;
    @TableField("best_treat_time")
    private String bestTreatTime;
    @TableField("treat_length")
    private String treatLength;
    @TableField("diagnosis")
    private String diagnosis;
    @TableField("preparation")
    private String preparation;
    @TableField("outpatien_treatment")
    private String outpatienTreatment;
    @TableField("other")
    private String other;


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIntroduction() {
        return introduction;
    }

    public void setIntroduction(String introduction) {
        this.introduction = introduction;
    }

    public String getIsInsurance() {
        return isInsurance;
    }

    public void setIsInsurance(String isInsurance) {
        this.isInsurance = isInsurance;
    }

    public String getAnotherName() {
        return anotherName;
    }

    public void setAnotherName(String anotherName) {
        this.anotherName = anotherName;
    }

    public String getSiteOfDisease() {
        return siteOfDisease;
    }

    public void setSiteOfDisease(String siteOfDisease) {
        this.siteOfDisease = siteOfDisease;
    }

    public String getInfectivity() {
        return infectivity;
    }

    public void setInfectivity(String infectivity) {
        this.infectivity = infectivity;
    }

    public String getTypeOfInfectivity() {
        return typeOfInfectivity;
    }

    public void setTypeOfInfectivity(String typeOfInfectivity) {
        this.typeOfInfectivity = typeOfInfectivity;
    }

    public String getTransmission() {
        return transmission;
    }

    public void setTransmission(String transmission) {
        this.transmission = transmission;
    }

    public String getCrowd() {
        return crowd;
    }

    public void setCrowd(String crowd) {
        this.crowd = crowd;
    }

    public String getSymptoms() {
        return symptoms;
    }

    public void setSymptoms(String symptoms) {
        this.symptoms = symptoms;
    }

    public String getConcurrentDisease() {
        return concurrentDisease;
    }

    public void setConcurrentDisease(String concurrentDisease) {
        this.concurrentDisease = concurrentDisease;
    }

    public String getIncubationPeriod() {
        return incubationPeriod;
    }

    public void setIncubationPeriod(String incubationPeriod) {
        this.incubationPeriod = incubationPeriod;
    }

    public String getPerformance() {
        return performance;
    }

    public void setPerformance(String performance) {
        this.performance = performance;
    }

    public String getDepartment() {
        return Department;
    }

    public void setDepartment(String Department) {
        this.Department = Department;
    }

    public String getCost() {
        return Cost;
    }

    public void setCost(String Cost) {
        this.Cost = Cost;
    }

    public String getCureRate() {
        return cureRate;
    }

    public void setCureRate(String cureRate) {
        this.cureRate = cureRate;
    }

    public String getTreatmentCycle() {
        return treatmentCycle;
    }

    public void setTreatmentCycle(String treatmentCycle) {
        this.treatmentCycle = treatmentCycle;
    }

    public String getTreatment() {
        return treatment;
    }

    public void setTreatment(String treatment) {
        this.treatment = treatment;
    }

    public String getCorrelation() {
        return Correlation;
    }

    public void setCorrelation(String Correlation) {
        this.Correlation = Correlation;
    }

    public String getMedicinal() {
        return medicinal;
    }

    public void setMedicinal(String medicinal) {
        this.medicinal = medicinal;
    }

    public String getOperation() {
        return operation;
    }

    public void setOperation(String operation) {
        this.operation = operation;
    }

    public String getBestTreatTime() {
        return bestTreatTime;
    }

    public void setBestTreatTime(String bestTreatTime) {
        this.bestTreatTime = bestTreatTime;
    }

    public String getTreatLength() {
        return treatLength;
    }

    public void setTreatLength(String treatLength) {
        this.treatLength = treatLength;
    }

    public String getDiagnosis() {
        return diagnosis;
    }

    public void setDiagnosis(String diagnosis) {
        this.diagnosis = diagnosis;
    }

    public String getPreparation() {
        return preparation;
    }

    public void setPreparation(String preparation) {
        this.preparation = preparation;
    }

    public String getOutpatienTreatment() {
        return outpatienTreatment;
    }

    public void setOutpatienTreatment(String outpatienTreatment) {
        this.outpatienTreatment = outpatienTreatment;
    }

    public String getOther() {
        return other;
    }

    public void setOther(String other) {
        this.other = other;
    }

    @Override
    public String toString() {
        return "Disease{" +
        ", name=" + name +
        ", introduction=" + introduction +
        ", isInsurance=" + isInsurance +
        ", anotherName=" + anotherName +
        ", siteOfDisease=" + siteOfDisease +
        ", infectivity=" + infectivity +
        ", typeOfInfectivity=" + typeOfInfectivity +
        ", transmission=" + transmission +
        ", crowd=" + crowd +
        ", symptoms=" + symptoms +
        ", concurrentDisease=" + concurrentDisease +
        ", incubationPeriod=" + incubationPeriod +
        ", performance=" + performance +
        ", Department=" + Department +
        ", Cost=" + Cost +
        ", cureRate=" + cureRate +
        ", treatmentCycle=" + treatmentCycle +
        ", treatment=" + treatment +
        ", Correlation=" + Correlation +
        ", medicinal=" + medicinal +
        ", operation=" + operation +
        ", bestTreatTime=" + bestTreatTime +
        ", treatLength=" + treatLength +
        ", diagnosis=" + diagnosis +
        ", preparation=" + preparation +
        ", outpatienTreatment=" + outpatienTreatment +
        ", other=" + other +
        "}";
    }
}
