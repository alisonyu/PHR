
package com.scau.phr.domain.entity;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.scau.phr.domain.ienum.Sex;

import java.io.Serializable;
import java.time.LocalDate;

public class User implements Serializable{
        private static final long serialVersionUID = 1L;
        @TableId
        private String telephone;
        private String personalAccountId;
        private String password;
        private String realName;
        private Sex sex;
        private String location;
        @TableField("is_verified")
        private Boolean verified;
        @TableField("is_vip")
        private Boolean vip;
        private String email;
        @TableField("nick_name")
        private String nickname;
        private String education;
        @TableField("id_card_no")
        private String idcard;
        private String job;
        @TableField("is_married")
        private Boolean marry;
        /**
         * 身高，单位是cm
         */
        @TableField("height")
        private Integer height;
        @TableField(value = "create_time", fill = FieldFill.INSERT)
        private LocalDate createTime;
        @TableField(value = "update_time", fill = FieldFill.INSERT_UPDATE)
        private LocalDate updateTime;

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public String getPersonalAccountId() {
        return personalAccountId;
    }

    public void setPersonalAccountId(String personalAccountId) {
        this.personalAccountId = personalAccountId;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getRealName() {
        return realName;
    }

    public void setRealName(String realName) {
        this.realName = realName;
    }

    public Sex getSex() {
        return sex;
    }

    public void setSex(Sex sex) {
        this.sex = sex;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public Boolean getVerified() {
        return verified;
    }

    public void setVerified(Boolean verified) {
        this.verified = verified;
    }

    public Boolean getVip() {
        return vip;
    }

    public void setVip(Boolean vip) {
        this.vip = vip;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getEducation() {
        return education;
    }

    public void setEducation(String education) {
        this.education = education;
    }

    public String getIdcard() {
        return idcard;
    }

    public void setIdcard(String idcard) {
        this.idcard = idcard;
    }

    public String getJob() {
        return job;
    }

    public void setJob(String job) {
        this.job = job;
    }

    public Boolean getMarry() {
        return marry;
    }

    public void setMarry(Boolean marry) {
        this.marry = marry;
    }

    public Integer getHeight() {
        return height;
    }

    public void setHeight(Integer height) {
        this.height = height;
    }

    public LocalDate getCreateTime() {
        return createTime;
    }

    public void setCreateTime(LocalDate createTime) {
        this.createTime = createTime;
    }

    public LocalDate getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(LocalDate updateTime) {
        this.updateTime = updateTime;
    }

    @Override
    public String toString() {
        return "User{" +
                "telephone='" + telephone + '\'' +
                ", personalAccountId='" + personalAccountId + '\'' +
                ", password='" + password + '\'' +
                ", realName='" + realName + '\'' +
                ", sex=" + sex +
                ", location=" + location +
                ", verified=" + verified +
                ", vip=" + vip +
                ", email='" + email + '\'' +
                ", nickname='" + nickname + '\'' +
                ", education='" + education + '\'' +
                ", idcard='" + idcard + '\'' +
                ", job='" + job + '\'' +
                ", marry=" + marry +
                ", createTime=" + createTime +
                ", updateTime=" + updateTime +
                '}';
    }
}