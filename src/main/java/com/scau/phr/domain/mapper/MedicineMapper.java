package com.scau.phr.domain.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.scau.phr.domain.entity.Medicine;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import java.util.List;

public interface MedicineMapper extends BaseMapper<Medicine> {

    /**
     * 查找所有表单（分页）
     * @return
     */
    @Select("select * " +
            "from " +
            "medicine " +
            "order by name ")
    public List<Medicine> queryAll(Page page);

    @Select(" select name" +
            " from medicine" +
            " order by name")
    public List<String> listMedicineName(Page page);


    /**
     * 传入Medicine的名称，通过名字模糊查询(分页)
     * 注意：只支持前缀的查询
     * @param name
     * @return
     */
    @Select(" SELECT * FROM medicine where name like #{medicineName}\"%\" " +
            " union " +
            " SELECT * FROM medicine where alias like #{medicineName}\"%\" " +
            " union " +
            " SELECT * FROM medicine where foreign_name like #{medicineName}\"%\" " +
            " order by name")
    public List<Medicine> searchByName(@Param(value = "medicineName")String name,Page page);


}
