package com.scau.phr.domain.mapper;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.scau.phr.domain.entity.HealthKnowledge;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.scau.phr.domain.entity.Newhospital;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author yuzhiyi
 * @since 2018-08-05
 */
public interface HealthKnowledgeMapper extends BaseMapper<HealthKnowledge> {
    /**
     * 获取全部新闻信息
     */
    @Select("select id,title,author,description,image,release_time from health_knowledge order by release_time desc")
    public List<HealthKnowledge> listAllHealthKnowledge(Page page);

    //根据id查找
    @Select("select * from health_knowledge where id = #{id}")
    public HealthKnowledge findHealthKnowledgeById(@Param("id") Long id);

    //根据类型查找
    @Select("select id,title,author,description,image,release_time from health_knowledge where type = #{type} order by release_time desc")
    public List<HealthKnowledge> findHealthKnowledgeByType(@Param("type") String type,Page page);

    @Select("select * from health_knowledge order by release_time desc")
    public List<HealthKnowledge> getAll();
}
