package com.scau.phr.domain.mapper;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.scau.phr.domain.entity.MedicineData;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * <p>
 *  药品Mapper 接口
 * </p>
 *
 * @author yuzhiyi
 * @since 2018-08-07
 */
public interface MedicineDataMapper extends BaseMapper<MedicineData> {

	/**
	 * 分页模糊搜索药品
	 * 药品只返回id，名称，图片链接和功效四个字段
	 * @param name 药品名称
	 * @param page 分页
	 * @return
	 */
	@Select("select id,name,image,efficacy from medicine_data where name like  \"%\"#{name}\"%\" order by name")
	public List<MedicineData> searchMedicineByName(@Param("name") String name, Page page);

}
