package com.scau.phr.domain.mapper;

import com.scau.phr.domain.entity.HealthRecord;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author yuzhiyi
 * @since 2018-08-05
 */
public interface HealthRecordMapper extends BaseMapper<HealthRecord> {
    @Select("select * from health_record where telephone = #{telephone}")
    public HealthRecord getHealthRecordByPhone(@Param("telephone") String telephone);


}
