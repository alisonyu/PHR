package com.scau.phr.domain.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.scau.phr.domain.entity.PhrMessage;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.time.LocalDateTime;
import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author yuzhiyi
 * @since 2018-08-13
 */
public interface PhrMessageMapper extends BaseMapper<PhrMessage> {


	@Select("select * from phr_message where receiver = #{tel} and send_time< now() order by send_time desc")
	public List<PhrMessage> listMessageByTel(@Param("tel") String tel);

	@Select("update phr_message set readed = 1 where id = #{id}")
	public void makeMessageReadedById(@Param("id") Long id);

	@Select("select * from phr_message where receiver = #{tel} and send_time >= #{time} and send_time < now()")
	public List<PhrMessage> listMessageAfterSendTime(@Param("tel") String tel ,@Param("time") String time);


}
