package com.scau.phr.domain.mapper;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.scau.phr.domain.entity.Newhospital;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.sun.org.apache.bcel.internal.generic.NEW;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author yuzhiyi
 * @since 2018-08-05
 */
public interface NewhospitalMapper extends BaseMapper<Newhospital> {


	/**
	 * 根据区拉取数据
	 * @param county 区名 ： 例如黄埔区
	 */
	@Select("select id,name from newhospital where address_county = #{county} order by name")
	public List<Newhospital> listHospitalByCounty(@Param("county") String county,@Param("city") String city,Page page);


	/**
	 * 根据市拉取数据
	 */
	@Select("select id,name from newhospital where address_city = #{city} order by name")
	public List<Newhospital> listHospitalByCity(@Param("city") String city,Page page);

	/**
	 * 根据省拉取数据
	 */
	@Select("select id,name from newhospital where address_province = #{province} order by name")
	public List<Newhospital> listHospitialByProvice(@Param("province")String province,Page page);

	/**
	 * 根据省跟市
	 * 获取医院经纬度信息
	 */
	@Select("select id,longitude,latitude from newhospital where address_province = #{province} and address_city = #{city}")
	public List<Newhospital> getHospitalLocation(@Param("province") String province,@Param("city") String city);

	/**
	 * 根据市
	 * 获取医院经纬度信息
	 */
	@Select("select id,longitude,latitude from newhospital where address_city = #{city}")
	public List<Newhospital> getHospitalLocation2(@Param("city") String city);


	@Select("select * from newhospital where address_province = '广东省'")
	public List<Newhospital> getHospitalInguangdong();

	@Select("select * from newhospital")
	public List<Newhospital> getAll();
}
