package com.scau.phr.domain.mapper;

import com.scau.phr.domain.entity.Recipe;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author yuzhiyi
 * @since 2018-08-05
 */
public interface RecipeMapper extends BaseMapper<Recipe> {

}
