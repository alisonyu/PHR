package com.scau.phr.domain.mapper;

import com.scau.phr.business.hosipital.domain.AppointmentInfoDTO;
import com.scau.phr.domain.entity.Appointment;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.time.LocalDate;
import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author yuzhiyi
 * @since 2018-08-05
 */
public interface AppointmentMapper extends BaseMapper<Appointment> {

	@Select("select count(1) from appointment where user_id = #{phone} and  hospital_id = #{hospital} and start_time = #{startTime}   ")
	public Integer queryExistedAppointment(@Param("phone") String phone,@Param("hospital") Long hospitalId,@Param("startTime") LocalDate startTime);

	@Select("SELECT \n" +
			"newhospital.name as hospital_name,newhospital.phone_number as hospital_tel,newhospital.address as hospital_address,\n" +
			"appointment.doctor,appointment.department,appointment.user_id as phone,appointment.start_time,appointment.stop_time\n" +
			"FROM\n" +
			"appointment join newhospital on appointment.hospital_id = newhospital.id\n" +
			"where user_id = #{userId} ")
	public List<AppointmentInfoDTO> getAppointHistory(@Param("userId") String userId);


	@Select("SELECT \n" +
			"newhospital.name as hospital_name,newhospital.phone_number as hospital_tel,newhospital.address as hospital_address,\n" +
			"appointment.doctor,appointment.department,appointment.user_id as phone,appointment.start_time,appointment.stop_time\n" +
			"FROM\n" +
			"appointment join newhospital on appointment.hospital_id = newhospital.id\n" +
			"where appointment_id = #{id} ")
	public AppointmentInfoDTO getAppointmentInfoByAppointmentId(@Param("id") Long appointmentId);


}
