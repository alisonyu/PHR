package com.scau.phr.domain.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.scau.phr.domain.entity.PhrBmi;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * @author yuzhiyi
 * @date 2018/8/14 9:58
 */
public interface PhrBmiMapper extends BaseMapper<PhrBmi> {

	@Select("select weight,time from phr_bmi where tel = #{tel} order by time desc")
	public List<PhrBmi> listWeightHistory(@Param("tel") String userTel);



}
