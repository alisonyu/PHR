package com.scau.phr.domain.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.scau.phr.domain.entity.User;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author yuzhiyi
 * @since 2018-07-10
 */
public interface UserMapper extends BaseMapper<User> {



    /**
     * 查找所有表单（分页）
     * @param page 分页记录
     * @return
     */
    @Select("select * " +
            "from " +
            "user " +
            "order by telephone "
            )
    public List<User> queryAll(IPage page);

    /**
     * 查询唯一记录
     * @param tel
     * @return
     */
    @Select("select * " +
            "from " +
            "user " +
            "where telephone=#{tel};")
    public User queryOne(String tel);


    /**
     * 传入User，通过名字模糊查询(分页)
     * @param page
     * @param name
     * @return
     */
    @Select(" select * " +
            " from user " +
            " where " +
            " real_name like #{name}\"%\") " +
            " order by tel ")
    public List<User> SearchByName(IPage page, @Param(value = "name") String name);


    @Update("update user set height = #{height} where telephone = #{tel} ")
    public void updateHeight(@Param("tel") String tel,@Param("height") Integer height);

    @Select("select height from user where telephone = #{tel} ")
    public Integer getHeightByTel(@Param("tel") String tel);

}
