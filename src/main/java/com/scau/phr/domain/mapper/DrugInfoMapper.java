package com.scau.phr.domain.mapper;

import com.scau.phr.domain.entity.DrugInfo;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author yuzhiyi
 * @since 2018-08-05
 */
public interface DrugInfoMapper extends BaseMapper<DrugInfo> {

}
