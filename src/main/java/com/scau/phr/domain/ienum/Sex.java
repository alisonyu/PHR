package com.scau.phr.domain.ienum;

import com.baomidou.mybatisplus.core.enums.IEnum;
import com.fasterxml.jackson.annotation.JsonValue;

import java.io.Serializable;

/**
 * 测试Enum
 * @author yuzhiyi
 * @date 2018/7/25 9:16
 */
@SuppressWarnings("AlibabaEnumConstantsMustHaveComment")
public enum Sex implements IEnum {
    MALE(1,"男"),
    FEMALE(2,"女")
    ;

    private int value;
    private String desc;

    Sex(int value, String desc) {
        this.value = value;
        this.desc = desc;
    }

    @Override
    public Serializable getValue() {
        return this.value;
    }

    @JsonValue
    public String getDesc(){
        return this.desc;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    @Override
    public String toString() {
        return desc;
    }
}
