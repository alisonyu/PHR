package com.scau.phr.common.exceptions;

/**
 * 在这里定义错误信息类别
 * @author yuzhiyi
 * @date 2018/7/9 10:51
 */
@SuppressWarnings("AlibabaEnumConstantsMustHaveComment")
public enum ErrorCode {
    SUCCESS(200,"成功"),
    NO_PERMISSION(211,"权限不足"),
    SERVER_ERROR(10000,"服务器异常"),
    AUTH_ERROR(10001,"认证失败"),
    PARAMS_ERROR(10002,"参数错误"),
    JSON_PARSE_ERROR(10003,"Json解析错误"),
    MSG_REQ_ERROR(10004,"短信服务请求失败"),
    MSG_REQ_TOO_FAST_ERROR(10005,"短信请求太快"),
    USER_NOT_REGISTER(10006,"用户尚未注册"),
    MSG_CODE_ERROR(10007,"验证码错误或过期"),
    AUTH_PASSWORD_ERROR(10008,"密码错误"),
    AUTH_USER_EXIST(10009,"用户已经存在"),
    APPOINT_DUPLICATE(11000,"一天内不能重复预约同一家医院"),
    DATABASE_OPERATE_ERRPR(15000,"数据库操作失败"),
    ILLEAGAL_STRING(15001,"非法字符串"),
    UNKNOW_ERROR(16000,"未知错误");


    private int code;
    private String msg;

    ErrorCode(int code,String msg){
        this.code = code;
        this.msg = msg;
    }

    public int getCode() {
        return code;
    }

    public String getMsg() {
        return msg;
    }
}
