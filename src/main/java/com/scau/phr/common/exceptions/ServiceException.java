package com.scau.phr.common.exceptions;

import java.util.HashMap;

/**
 * 业务逻辑异常，当业务逻辑出现错误的时候直接抛出该异常即可
 * 框架会将异常包装成ApiResult的Json返回给客户端
 * @author yuzhiyi
 * @date 2018/7/9 10:51
 */
public class ServiceException extends RuntimeException {
    private ErrorCode errorCode;
    private String message;
    private Object value;

    private static final Object NULL_VALUE = new HashMap(0);

    public ServiceException(ErrorCode errorCode,String message,Object value){
        this.errorCode = errorCode;
        this.message = message;
        this.value = value;
    }

    public ServiceException(ErrorCode errorCode,String message){
        this.errorCode = errorCode;
        this.message = message;
        this.value = NULL_VALUE;
    }

    public ServiceException(ErrorCode errorCode){
        this.errorCode = errorCode;
        if (errorCode==ErrorCode.SUCCESS){
            this.message = "";
        }else{
            this.message = "";
        }
        this.value = NULL_VALUE;
    }

    public ErrorCode getErrorCode() {
        return errorCode;
    }

    @Override
    public String getMessage() {
        return message;
    }

    public Object getValue() {
        return value;
    }
}
