package com.scau.phr.common.util;

import com.scau.phr.common.exceptions.ServiceException;

import static com.scau.phr.common.exceptions.ErrorCode.ILLEAGAL_STRING;
import static org.springframework.util.StringUtils.split;

public class StrTool {
    /**
     * 非法字符集
     */
    private String CHECKSQL = "^(.+)\\sand\\s(.+)|(.+)\\sor(.+)\\s$";
    /**
     * 检测非法字符
     * @param str
     * @return
     */
    public static boolean sql_inj(String str)throws ServiceException {

        String inj_str = "'|and|exec|insert|select|delete|update|count|*|%|chr|mid|master|truncate|char|declare|;|or|-|+|,";
        String inj_stra[] = split(inj_str,"|");
        for (int i=0 ; i<inj_stra.length ; i++ ){
            if (str.indexOf(inj_stra[i])>=0){
                throw new ServiceException(ILLEAGAL_STRING,inj_stra[i]);
            }
        }
        return true;
    }
}
