package com.scau.phr.common.util;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;

/**
 * @author yuzhiyi
 * @date 2018/8/7 11:44
 */
public class PageUtil {

	/**
	 * 一页的数据项数
	 */
	private static final int PAGE_AMOUNT = 10;

	public static Page getPage(int i){
		if (i<=0){
			return getDefaultPage();
		}
		return new Page(i,PAGE_AMOUNT);
	}

	private static Page getDefaultPage(){
		return new Page(1,10);
	}
}
