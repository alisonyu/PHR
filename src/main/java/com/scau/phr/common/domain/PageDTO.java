package com.scau.phr.common.domain;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;

import java.beans.Transient;
import java.util.ArrayList;
import java.util.List;

/**
 * @author yuzhiyi
 * @date 2018/8/7 10:16
 */
public class PageDTO<T> {

	/**
	 * 分页结果
	 */
	private List<T> value;
	/**
	 * 最大页数
	 */
	private Long maxPage;
	/**
	 * 当前页数
	 */
	private Long currentPage;
	/**
	 * 空结果
	 */
	private static final PageDTO EMPTY_RESULT;

	static {
		EMPTY_RESULT = new PageDTO();
		EMPTY_RESULT.value = new ArrayList();
		EMPTY_RESULT.maxPage = 0L;
		EMPTY_RESULT.currentPage = 0L;
	}

	public static <T>PageDTO<T> valueOf(List<T> res,Page page){
		PageDTO<T> dto = new PageDTO<>();
		dto.currentPage = page.getCurrent();
		dto.maxPage = page.getTotal() / page.getSize();
		dto.value = res;
		return dto;
	}

	public static PageDTO getEmptyResult(){
		return EMPTY_RESULT;
	}

	public List<T> getValue() {
		return value;
	}

	public void setValue(List<T> value) {
		this.value = value;
	}

	public Long getMaxPage() {
		return maxPage;
	}

	public void setMaxPage(Long maxPage) {
		this.maxPage = maxPage;
	}

	public Long getCurrentPage() {
		return currentPage;
	}

	public void setCurrentPage(Long currentPage) {
		this.currentPage = currentPage;
	}
}
