package com.scau.phr.common.db;

import com.mysql.cj.core.exceptions.DataReadException;

import javax.sql.DataSource;
import java.sql.*;
import java.util.ArrayList;
import java.util.Arrays;

/**
 * 获取所有表明
 * @author yuzhiyi
 * @date 2018/7/10 10:19
 */
public class SchemaHelper {

    static {
        try {
            Class.forName(com.mysql.jdbc.Driver.class.getName());
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }


    public static String[] getTablesName(String url,String username,String password,String schema) throws SQLException {
        Connection conn = DriverManager.getConnection(url,username,password);
        String sql = "select table_name from information_schema.tables where table_schema= ? and table_type='base table'";
        PreparedStatement stmt = conn.prepareStatement(sql);
        stmt.setString(1,schema);
        ResultSet resultSet = stmt.executeQuery();
        ArrayList<String> list = new ArrayList<>();
        while(resultSet.next()){
            list.add(resultSet.getString("table_name"));
        }
        resultSet.close();
        stmt.close();
        conn.close();
        String[] res = list.toArray(new String[]{});
        return res;
    }

    public static void main(String[] args) throws SQLException {
        String url = "jdbc:mysql://119.23.65.203:3306/stumanager?useUnicode=true&characterEncoding=utf8&useSSL=false";
        String user = "root";
        String password = "123456";
        String name = "medicine";
        String[] tables = getTablesName(url,user,password,name);
        System.out.println(Arrays.toString(tables));
    }


}
