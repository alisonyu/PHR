package com.scau.phr.common.db;
import com.baomidou.mybatisplus.annotation.DbType;
import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.generator.AutoGenerator;
import com.baomidou.mybatisplus.generator.config.*;
import com.baomidou.mybatisplus.generator.config.po.TableFill;
import com.baomidou.mybatisplus.generator.config.rules.NamingStrategy;
import com.google.common.collect.Lists;

import java.sql.SQLException;
import java.util.List;

/**
 * 自动生成数据库代码
 * @author yuzhiyi
 * @date 2018/7/10 10:19
 */
public class MpGenerator {

    private static String url = "jdbc:mysql://119.23.65.203:3306/phr?serverTimezone=CTT";
    private static String user = "root";
    private static String password = "123456";
    private static String name = "phr";
    private static String author = "yuzhiyi";
    private static String outputDir = "d:\\codeGen";
    private static String PackageName = "com.scau.phr.domain";

    /**
     * MySQL 生成演示
     */
    public static void main(String[] args) {
        MpGenerator mpGenerator = new MpGenerator();
        //mpGenerator.generateAllTables(PackageName);
        mpGenerator.generateByTables("com.scau.phr.common.domain.entity","phr_bmi");
    }

    private void generateAllTables(String packageName){
        try {
            generateByTables(packageName,SchemaHelper.getTablesName(url,user,password,name));
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private void generateByTables(boolean serviceNameStartWithI, String packageName, String... tableNames) {
        /**全局配置 begin */
        GlobalConfig config = new GlobalConfig();
        //设置主键默认不填充
        config.setIdType(IdType.NONE)
                .setActiveRecord(false)
                .setAuthor(author)
                .setOutputDir(outputDir)
                .setFileOverride(true);
        /** 全局配置 end */

        /**数据源配置 begin */
        String dbUrl = url;
        DataSourceConfig dataSourceConfig = new DataSourceConfig();
        dataSourceConfig
                .setDbType(DbType.MYSQL)
                .setUrl(dbUrl)
                .setUsername(user)
                .setPassword(password)
                .setDriverName("com.mysql.jdbc.Driver");
        /**数据源配置 end*/

        /**代码生成策略 begin*/
        StrategyConfig strategyConfig = new StrategyConfig();
        //自动填充字段
        List<TableFill> tableFillList = Lists.newArrayList(
                new TableFill("create_time", FieldFill.INSERT),
                new TableFill("update_time",FieldFill.INSERT_UPDATE)
                );

        strategyConfig
                //大写命名
                .setCapitalMode(true)
                //数据库命名是以下划线_命名的
                .setDbColumnUnderline(true)
                //生成的字段带有注解@TableField
                .entityTableFieldAnnotationEnable(true)
                //将数据库的Boolean字段(即tinyint(1))is_xxx映射成xxx
                .setEntityBooleanColumnRemoveIsPrefix(true)
                //将下划线转化为驼峰命名
                .setNaming(NamingStrategy.underline_to_camel)
                //指定自动填充字段
                .setTableFillList(tableFillList)
                //修改替换成你需要的表名，多个表名传数组
                .setInclude(tableNames);

        if (!serviceNameStartWithI) {
            config.setServiceName("%sService");
        }
        /**代码生成策略 end*/
        new AutoGenerator().setGlobalConfig(config)
                .setDataSource(dataSourceConfig)
                .setStrategy(strategyConfig)
                .setPackageInfo(
                        new PackageConfig()
                                .setParent(packageName)
                                .setController("controller")
                                .setEntity("entity")
                ).execute();
    }

    private void generateByTables(String packageName, String... tableNames) {
        generateByTables(true, packageName, tableNames);
    }

}