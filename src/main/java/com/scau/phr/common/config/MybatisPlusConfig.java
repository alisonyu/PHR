package com.scau.phr.common.config;

import com.baomidou.mybatisplus.extension.plugins.PaginationInterceptor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * MyBatis Plus 配置
 * @author yuzhiyi
 * @date 2018/7/27 14:53
 */
@Configuration
@EnableTransactionManagement
public class MybatisPlusConfig {

    /**
     * 加载分页插件
     */
    @Bean
    public PaginationInterceptor paginationInterceptor(){
        return new PaginationInterceptor();
    }

}
