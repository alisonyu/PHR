package com.scau.phr.common.config;

import com.baomidou.mybatisplus.core.handlers.MetaObjectHandler;
import org.apache.ibatis.reflection.MetaObject;
import org.springframework.stereotype.Component;

import java.time.LocalDate;

/**
 * MyBatis公共字段填充
 * 自动填充create_time,在插入的时候填充localDate.now()
 * 自动填充update_time，在插入和更新的时候填充localDate.now()
 * @author yuzhiyi
 * @date 2018/7/25 17:19
 */
@Component
public class CommonTimeFillHandler extends MetaObjectHandler {

    private static final String CREATE_TIME = "createTime";
    private static final String UPDATE_TIME = "updateTime";

    @Override
    public void insertFill(MetaObject metaObject) {
        setFieldValByName(CREATE_TIME, LocalDate.now(),metaObject);
        setFieldValByName(UPDATE_TIME,LocalDate.now(),metaObject);
    }

    @Override
    public void updateFill(MetaObject metaObject) {
        setFieldValByName(UPDATE_TIME,LocalDate.now(),metaObject);
    }
}
