package com;

import com.scau.phr.sdk.baiduMap.domain.HospitalCoordinate;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Created with IDEA
 * author:scaurb
 * Date:2018/8/13/013
 * Time:10:55
 **/
public class testSort {
    public static void main(String[] args) {
        List<HospitalCoordinate> list = null;
        list.add(new HospitalCoordinate(1.0,1.0));
        list.add(new HospitalCoordinate(1.0,2.0));
        list.add(new HospitalCoordinate(2.0,2.0));
        list.add(new HospitalCoordinate(2.0,3.0));
        list.add(new HospitalCoordinate(2.0,1.0));
        list.add(new HospitalCoordinate(3.0,3.0));
        Collections.sort(list , new Comparator<HospitalCoordinate>(){

            @Override
            public int compare(HospitalCoordinate o1, HospitalCoordinate o2) {
                if(o1.getLongitude() < o1.getLongitude()){
                    return 1;
                }else if(o1.getLongitude() == o1.getLongitude()){
                    if(o1.getLatitude() < o1.getLatitude()){
                        return 1;
                    }else{
                        return 0;
                    }
                }else{
                    return 0;
                }
            }
        });
        for (HospitalCoordinate item:list) {
            System.out.println(item.getLongitude() + " " + item.getLatitude());
        }

    }
}
