package com.scau.phr.business.message.service;

import com.scau.phr.ServerBootstrap;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.LocalDateTime;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest(
		classes = ServerBootstrap.class
)
public class MessageServiceTest {

	@Autowired
	MessageService messageService;

	@Test
	public void addMessage() {
		messageService.addMessage("管理员","13602804849","hello world2","系统消息", LocalDateTime.of(2018,9,20,0,0,0));
	}

	@Test
	public void queryAllMessage(){
		System.out.println(messageService.getAllMessage("13602804849"));
	}


}