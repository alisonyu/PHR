package com.scau.phr.business.bmi.service;

import com.scau.phr.ServerBootstrap;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.LocalDateTime;

import static org.junit.Assert.*;
@RunWith(SpringRunner.class)
@SpringBootTest(
		classes = ServerBootstrap.class
)
public class BmiServiceTest {

	@Autowired
	BmiService service;

	@Test
	public void getWeightHistory() {

		System.out.println(service.getWeightHistory("13602804849"));

	}

	@Test
	public void addWeightRecord() {

		service.addWeightRecord("13602804849",65.0);

	}

	@Test
	public void addWeightRecord1() {
		LocalDateTime dateTime = LocalDateTime.of(2017,6,1,12,0);
		service.addWeightRecord("13602804849",65.0,dateTime);
	}

	@Test
	public void updateHeight() {

		service.updateHeight("13602804849",173);

	}
}