package com.scau.phr.sdk.baiduMap;

import com.scau.phr.ServerBootstrap;
import com.scau.phr.sdk.baiduMap.domain.HospitalCoordinate;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static org.junit.Assert.*;

/**
 * Created with IDEA
 * author:scaurb
 * Date:2018/8/13/013
 * Time:11:54
 **/
@RunWith(SpringRunner.class)
@SpringBootTest(
        classes = ServerBootstrap.class
)
public class MyAddressUtilTest {

    @Autowired
    MyAddressUtil util;

    @Test
    public void getNearHospital() throws Exception {
        List<HospitalCoordinate> list = util.getNearHospital("潮州市",116,23);
        System.out.println(list);
    }

}