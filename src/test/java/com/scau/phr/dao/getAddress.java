package com.scau.phr.dao;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.List;
import java.util.Map;

/**
 * Created with IDEA
 * author:scaurb
 * Date:2018/8/9/009
 * Time:10:40
 **/
public class getAddress {
    public static String SendGET(String url,String param){
        String result="";//访问返回结果
        BufferedReader read=null;//读取访问结果

        try {
            //创建url
            URL realurl=new URL(url+"?"+param);
            //打开连接
            URLConnection connection=realurl.openConnection();
            // 设置通用的请求属性
            connection.setRequestProperty("accept", "*/*");
            connection.setRequestProperty("connection", "Keep-Alive");
            connection.setRequestProperty("user-agent",
                    "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.62 Safari/537.36");
            //建立连接
            connection.connect();
            // 获取所有响应头字段
            Map<String, List<String>> map = connection.getHeaderFields();
            // 遍历所有的响应头字段，获取到cookies等
            /*for (String key : map.keySet()) {
                System.out.println(key + "--->" + map.get(key));
            }*/
            // 定义 BufferedReader输入流来读取URL的响应
            read = new BufferedReader(new InputStreamReader(
                    connection.getInputStream(),"UTF-8"));
            String line;//循环读取
            while ((line = read.readLine()) != null) {
                result += line;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }finally{
            if(read!=null){//关闭流
                try {
                    read.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        return result;
    }

    public static void main(String[] args) {
        String url = "http://api.map.baidu.com/geocoder/v2/";
        String param = "address=北京市海淀区上地十街10号&output=json&ak=OglDSmakHcYgw3MHcNMIEvB1Qfd3P1o9&callback=showLocation";
        String result = getAddress.SendGET(url,param);
        result.split("[{ }]");
        String Location = null;
        for (String item: result.split("[{ }]")) {
            //System.out.println(item);
            if(item.contains("lng")&&item.contains("lat")){
                Location = item;
                break;
            }
        }
        System.out.println(Location);
        String lng = Location.split(",")[0];
        String lat = Location.split(",")[1];
        System.out.println(lng + "\n\r" + lat);
    }
}
