import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import com.google.common.cache.LoadingCache;
import org.junit.Test;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.concurrent.TimeUnit;

import static org.junit.Assert.*;

/**
 * 测试guava的缓存
 * @author yuzhiyi
 * @date 2018/7/17 20:52
 */
public class GuavaCacheTest {

    Cache<String,String> cache;

    public GuavaCacheTest(){
        cache = CacheBuilder.newBuilder()
                .maximumSize(1000)
                .expireAfterWrite(2, TimeUnit.MINUTES)
                .build();
    }

    @Test
    public void testExpired() throws InterruptedException {
        cache.put("123","000");
        TimeUnit.MINUTES.sleep(2);
        assertNull(cache.getIfPresent("123"));
    }

    @Test
    public void testDate(){
        System.out.println(LocalDateTime.now().format(DateTimeFormatter.ISO_DATE_TIME));
    }

    @Test
    public void testDate2(){
        LocalDateTime date =  LocalDateTime.parse("2018-12-4T12:00:23");
        System.out.println(date.format(DateTimeFormatter.ISO_DATE_TIME));
    }



}
