/*==============================================================*/
/* DBMS name:      MySQL 5.0                                    */
/* Created on:     2018/7/10 14:44:53                           */
/*==============================================================*/


drop table if exists User;

drop table if exists admin;

drop table if exists application_service_provider;

drop table if exists consultation_remind;

drop table if exists doctor;

drop table if exists drug_info;

drop table if exists health_info;

drop table if exists health_knowledge;

drop table if exists health_record;

drop table if exists immunization_plan;

drop table if exists passenger;

drop table if exists registration;

drop table if exists service_evaluation;

drop table if exists treatment_recoed;

drop table if exists vip_consume_record;

drop table if exists vip_expiration;

/*==============================================================*/
/* Table: User                                                  */
/*==============================================================*/
create table User
(
   tel                  char(25) not null,
   password             varchar(100) not null,
   real_name            char(25),
   sex                  int not null,
   add_time             date not null,
   location             int,
   is_verify            bool not null,
   is_vip               bool not null,
   email                char(100),
   nickname             char(25),
   education            char(10),
   job                  char(20),
   marry                int,
   update_time          date,
   primary key (tel)
);

/*==============================================================*/
/* Table: admin                                                 */
/*==============================================================*/
create table admin
(
   tel                  char(25) not null,
   password             varchar(100) not null,
   realname             char(25),
   registered_date      date not null,
   email                char(100),
   nickname             char(25),
   power                varchar(200),
   username             char(50),
   primary key (tel)
);

alter table admin comment 'PHR管理员';

/*==============================================================*/
/* Table: application_service_provider                          */
/*==============================================================*/
create table application_service_provider
(
   id                   char(25) not null,
   Name                 char(25) not null,
   primary key (id)
);

/*==============================================================*/
/* Table: consultation_remind                                   */
/*==============================================================*/
create table consultation_remind
(
   id                   char(30) not null,
   tel                  char(25) not null,
   hospital             char(30),
   department           char(30),
   date                 date,
   primary key (id)
);

/*==============================================================*/
/* Table: doctor                                                */
/*==============================================================*/
create table doctor
(
   tel                  char(25) not null,
   password             varchar(100) not null,
   realname             char(25),
   sex                  char(10) not null,
   register_time        date not null,
   hospital             char(100),
   is_verify            bool not null,
   email                char(100),
   nickname             char(25),
   job                  char(20),
   certificate_id        char(27),
   exp                  int,
   introduction         varchar(200),
   update_time          date,
   working_location     char(50),
   education            char(30),
   title                char(30),
   primary key (tel)
);

/*==============================================================*/
/* Table: drug_info                                             */
/*==============================================================*/
create table drug_info
(
   id                   char(50) not null,
   type                 char(50),
   name                 char(50),
   ename                char(50),
   requirement          char(50),
   packing              char(50),
   purpose              text,
   composition          text,
   properties           text,
   drug_usage              text,
   primary key (id)
);

/*==============================================================*/
/* Table: health_info                                           */
/*==============================================================*/
create table health_info
(
   id                   char(50) not null,
   title                char(50),
   type                 int,
   author               char(30),
   inputDate            date,
   content              text,
   visits               int,
   primary key (id)
);

/*==============================================================*/
/* Table: health_knowledge                                      */
/*==============================================================*/
create table health_knowledge
(
   id                   char(50) not null,
   title                char(50),
   type                 char(50),
   author               char(50),
   add_date             date,
   content              text,
   visits               int,
   primary key (id)
);

/*==============================================================*/
/* Table: health_record                                         */
/*==============================================================*/
create table health_record
(
   id                   char(50) not null,
   tel                  char(25),
   organization         char(50),
   doctor               char(50),
   add_time             date,
   occupation           char(50),
   marital_status       int,
   payer                char(50),
   drug_allergy_history text,
   expousre_history     text,
   disease_history      text,
   disability           text,
   symptom              text,
   temperature          char(10),
   pulse_rate           char(10),
   respiratory_rate     char(10),
   waistline            decimal(5,2),
   constitutional_index decimal(5,2),
   exercise             text,
   eating_habits        text,
   smoking              text,
   drinking             text,
   mouth                text,
   vision               text,
   hearing              text,
   sporting_ability     text,
   eyeground            text,
   skin                 text,
   lymph_gland          text,
   lung                 text,
   heart                text,
   abdomen              text,
   living_env  		text,
   primary key (id)
);

/*==============================================================*/
/* Table: immunization_plan                                     */
/*==============================================================*/
create table immunization_plan
(
   id                   char(25) not null,
   tel                  char(25) not null,
   start_time           date,
   primary key (id)
);

/*==============================================================*/
/* Table: passenger                                             */
/*==============================================================*/
create table passenger
(
   id                   char(25) not null,
   visit_Date           date not null,
   primary key (id)
);

/*==============================================================*/
/* Table: registration                                          */
/*==============================================================*/
create table registration
(
   id           char(30) not null,
   tel                  char(25) not null,
   hospital             char(30),
   department           char(30),
   primary key (id)
);

/*==============================================================*/
/* Table: service_evaluation                                    */
/*==============================================================*/
create table service_evaluation
(
   id                   char(50) not null,
   tel                  char(25),
   content              varchar(1024),
   primary key (id)
);

/*==============================================================*/
/* Table: treatment_record                                      */
/*==============================================================*/
create table treatment_record
(
   id          char(25) not null,
   tel                  char(25),
   primary key (id)
);

/*==============================================================*/
/* Table: vip_consume_record                                    */
/*==============================================================*/
create table vip_consume_record
(
   id      			char(30) not null,
   tel              char(25) not null,
   amount 			decimal(5,2),
   comsume_date    date,
   primary key (id)
);

/*==============================================================*/
/* Table: vip_expiration                                        */
/*==============================================================*/
create table vip_expiration
(
   tel                  char(25) not null,
   expire_time          date,
   primary key (tel)
);


